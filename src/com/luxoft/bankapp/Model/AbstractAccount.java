package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Annotations.NoDB;

import java.util.Map;

public abstract class AbstractAccount implements Account {
    @NoDB private int id;
    private float balance;

    public AbstractAccount() {
    }

    public AbstractAccount(int id) {
        this.id = id;
    }

    public AbstractAccount(float initialBalance, int id) {
        this(id);
        if (initialBalance < 0) {
            throw new IllegalArgumentException();
        }
        balance = initialBalance;
    }

    @Override
    public int getId() {
        return id;
    }

    @Override
    public int decimalValue() {
        return Math.round(balance);
    }

    @Override
    public float getBalance() {
        return balance;
    }

    @Override
    public void parseFeed(Map<String, String> feed) {
    }

    @Override
    public void setId(int id) {
        this.id = id;
    }

/// for task10
    public void setBalance(float f) {
        if (f < 0) {
            throw new IllegalArgumentException();
        }
        balance = f;
    }
}








