<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="static-content/bank.css">
    <%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
    <title>Balance</title>
</head>

<body>
<br><br><br>
<div class="center">
<h1>Current balance for <%= session.getAttribute("clientName")%>: <%= request.getAttribute("balance")%>
    <% if (request.getAttribute("overdraft") != null ) { %>
        <br><br>Available overdraft: <%= request.getAttribute("overdraft") %>
    <% } %>
</h1>
    <a href="menu.html">return to menu</a>
</div>
</body>
</html>