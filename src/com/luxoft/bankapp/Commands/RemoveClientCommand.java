package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.ServiceFactory;

public class RemoveClientCommand extends AbstractCommand {
    public RemoveClientCommand() {
        super("Remove active client");
    }

    @Override
    public void execute(BankCommander commander) throws BankException, ServiceException  {
        if (BankCommander.currentBank == null) {
            System.out.println("Error! Current bank is undefined\n");
            return;
        }
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Active client is undefined\n");
            return;
        }
        Client client = commander.getCurrentClient();
        ServiceFactory.getBankService().removeClient(commander.currentBank, client);
        commander.setCurrentClient(null);
        System.out.println("OK! Client is removed: " + client.getClientSalutation());
    }
}




