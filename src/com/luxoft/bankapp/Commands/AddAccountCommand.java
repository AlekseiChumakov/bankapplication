package com.luxoft.bankapp.Commands;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;

import java.sql.Types;

public class AddAccountCommand extends AbstractCommand {
    public AddAccountCommand() {
        super("Add account");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException, BankException {
        if (BankCommander.currentBank == null) {
            System.out.println("Error! Current bank is undefined");
            return;
        }
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined");
            return;
        }
        System.out.println("Current bank: " + BankCommander.currentBank.getName());
        System.out.println("Current client: " + commander.getCurrentClient().getClientSalutation());
        String accountType;
        while (true) {
            accountType = fromConsole("Input type of account ( S/C ): ");
            if (accountType.matches("[SsCc]")) {
                break;
            }
        }
        float balance;
        while (true) {
            try {
                balance = Float.parseFloat(fromConsole("Input start balance: "));
                if (balance < 0) {
                    System.out.println("Error! Value must be positive\n");
                    continue;
                }
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal balance value\n");
                continue;
            }
            break;
        }
        Account account;
        if (accountType.matches("[Ss]")) {
            account = new SavingAccount(balance, Types.NULL);
        } else {
            float initialOverdraft;
            while (true) {
                try {
                    initialOverdraft = Float.parseFloat(fromConsole("Input initial overdraft: "));
                    if (initialOverdraft < 0) {
                        System.out.println("Error! Value must be positive\n");
                        continue;
                    }
                } catch (RuntimeException e) {
                    System.out.println("Error! Illegal overdraft value\n");
                    continue;
                }
                break;
            }
            account = new CheckingAccount(balance, initialOverdraft, Types.NULL);
        }
        ServiceFactory.getClientService().saveAccount(commander.getCurrentClient(), new Account[]{account});
        System.out.println("Account created successfully!");
    }
}

