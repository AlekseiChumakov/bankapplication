<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="static-content/bank.css">
    <script src="static-content/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="static-content/validation.js"></script>
    <%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Добавление клиента</title>
</head>

<body>
<h1>Пожалуйста, заполните форму для ${empty client.id? "добавления" : "изменения"} клиента:</h1><br><br>
<form action="saveclient" method="POST">
    <input type="hidden" name="id" value="${client.id}">
<table id="addclient">
    <tr><td>Полное имя:</td><td><input type="text" name="name" value="${client.name}" onfocus="hideHints()" id="name"></td>
        <td><div id="nameError" class="hints"></div></td></tr>
    <tr><td>Город:</td><td><input type="text" name="city" value="${client.city}" onfocus="hideHints()" id="city"></td>
        <td><div id="cityError" class="hints"></div></td></tr>
    <tr><td>Пол:</td><td><input type="radio" name="gender" value="M"  ${client.gender=="MALE" || empty client.gender?"checked":""}>Мужской
        <input type="radio" name="gender" ${client.gender=="FEMALE"?"checked":""} value="F">Женский</td></tr>
    <tr><td>E-mail:</td><td><input type="text" name="email" value="${client.email}" id="email" onfocus="hideHints()"></td>
        <td><div id="emailError" class="hints"></div></td></tr>
    <tr><td>Кредитный счет:</td><td><input type="checkbox" name="credit">открыть</td></tr>
    <tr><td>Начальный баланс:</td><td><input type="text" name="balance" value="${client.activeAccount.balance}" onfocus="hideHints()" id="balance"></td>
        <td><div id="balanceError" class="hints"></div></td></tr>
</table><br>
<input type="submit" onclick="return checkAddClientForm();" value=${empty client.id?"Добавить клиента":"Сохранить"}>
</form>
</body>
</html>