package com.luxoft.bankapp.Model;

public enum Gender {
    MALE("Mr. "), FEMALE("Ms. ");

    Gender(String str) {
        s = str;
    }

    private final String s;

    public String convertToString() {
        return s;
    }
    public String getGenderChar() {
        if (this == Gender.MALE) {
            return "M";
        } else {
            return "F";
        }
    }
}








