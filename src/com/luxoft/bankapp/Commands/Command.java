package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Exceptions.BankException;

public interface Command {
    void execute(BankCommander commander) throws BankException, ServiceException;
    void printCommandInfo();
}








