package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.util.List;
import java.util.Set;

public interface BankService {
    Bank getBankByName(String name) throws ServiceException;
    void saveClient(Bank bank, Client[] clients) throws ServiceException;
    void removeClient(Bank bank, Client client) throws ServiceException, ClientNotFoundException;
    Client getClient(Bank bank, String clientName) throws ServiceException, ClientNotFoundException;
    Client getClient(Bank bank, int id) throws ServiceException, ClientNotFoundException;
    Set<Client> getAllClients(Bank bank) throws ServiceException;
    void saveClientToFile(Client c);
    Client loadClientFromFile();
    BankInfo getBankInfo(Bank bank) throws ServiceException;
    void clearBank() throws ServiceException;
    void save(Bank bank) throws ServiceException;
    List<Client> findClientsByCity(Bank bank, String city) throws ServiceException;
}








