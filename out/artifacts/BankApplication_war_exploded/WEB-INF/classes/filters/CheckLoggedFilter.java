import javax.servlet.*;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;

public class CheckLoggedFilter implements Filter {
    @Override
    public void doFilter(ServletRequest request, ServletResponse response, FilterChain chain)
        throws ServletException, IOException {

        String path = ((HttpServletRequest)request).getRequestURI();
        HttpServletResponse resp = (HttpServletResponse)response;

        HttpSession session = ((HttpServletRequest)request).getSession();
        String clientName = (String)session.getAttribute("clientName");

        if (path.startsWith("/static-content/") || path.startsWith("/login") || path.startsWith("/welcome") || path.equals("/") || clientName != null) {
            chain.doFilter(request, response);
        } else {
            resp.sendRedirect("/login.html");
        }
    }

    @Override
    public void destroy() {

    }

    @Override
    public void init(FilterConfig config) {

    }
}
