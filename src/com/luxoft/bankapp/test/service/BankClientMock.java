package com.luxoft.bankapp.test.service;

import com.luxoft.bankapp.ClientServer.BankServer;
import com.luxoft.bankapp.Model.Client;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import java.util.Date;
import java.util.concurrent.Callable;

public class BankClientMock implements Callable {
    static final String SERVER = "localhost";
    static final String clientType = "BANK CLIENT";
    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private int bankClientNumber;
    private Client client;
    private float amount;
    private long startTime;

    public BankClientMock(Client client, float amount, int n) {
        bankClientNumber = n;
        this.amount = amount;
        this.client = client;
        startTime = new Date().getTime();
    }

    @Override
    public Object call() {
        try {
            requestSocket = new Socket(SERVER, 2004);

            in = new ObjectInputStream(requestSocket.getInputStream());
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            if (!((String)in.readObject()).equals(BankServer.readyPrefix)) {
                return -1;
            }
            System.out.println("Client №" + bankClientNumber + " is running");

            String message;
            message = BankServer.withdrawPrefix + BankServer.separator +
                    client.getName() + BankServer.separator + amount;
            out.writeObject(message);
            out.flush();
            message = (String) in.readObject();
            if (message.startsWith(BankServer.okPrefix)) {
                System.out.println("Client №" + bankClientNumber + " OK! " + message.substring(BankServer.okPrefix.length()));
            } else {
                System.out.println("Client №" + bankClientNumber + " Error! " + message.substring(BankServer.errorPrefix.length()));
            }

            message = BankServer.exitPrefix + BankServer.separator;
            out.writeObject(message);
            out.flush();
            message = (String)in.readObject();
            if (message.startsWith(BankServer.okPrefix)) {
                System.out.println(message.substring(BankServer.okPrefix.length()));
            } else {
                System.out.println("Client №" + bankClientNumber + " Error! " + message.substring(BankServer.errorPrefix.length()));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (requestSocket != null) {
                    requestSocket.close();
                }
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
        return new Date().getTime() - startTime;
    }
}






