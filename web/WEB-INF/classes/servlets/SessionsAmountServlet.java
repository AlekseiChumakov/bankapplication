import javax.servlet.ServletContext;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

public class SessionsAmountServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        final ServletContext context = getServletContext();
        Integer clientsConnected = (Integer)context.getAttribute("clientsConnected");
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("text/html; charset=UTF-8");
        out.println("Sessions amount: " + clientsConnected);
    }
}
