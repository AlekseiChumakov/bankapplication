package src.com.luxoft.bankapp.services;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;
import com.luxoft.bankapp.test.service.TestService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BankServiceTest {
    Bank bank;
    Client client1;
    Client client2;
    CheckingAccount account1;
    SavingAccount account2;

    @Before
    public void initBank() throws ServiceException, BankException {
        ServiceFactory.getBankService().clearBank();
        bank = new Bank("My Bank");
        client1 = new Client();
        client1.setName("Mark");
        client1.setGender(Gender.MALE);
        client2 = new Client();
        client2.setName("Maria");
        client2.setGender(Gender.FEMALE);
        account1 = new CheckingAccount();
        client1.addAccount(account1);
        account2 = new SavingAccount();
        client2.addAccount(account2);
        bank.addClient(client1);
        bank.addClient(client2);
        account1.setBalance(0);
        account1.setOverdraftStartValue(1000f);
        account1.setOverdraftCurrentValue(700f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);
    }

    @Test
    public void getBankByNameTest() throws Exception {
        Bank bank2 = ServiceFactory.getBankService().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank, bank2));
    }

    @Test
    public void saveNewClientTest() throws Exception {
        Client client = new Client();
        client.setName("Marina");
        client.setGender(Gender.FEMALE);
        SavingAccount account = new SavingAccount();
        account.setBalance(1000f);
        client.addAccount(account);
        ServiceFactory.getBankService().saveClient(bank, new Client[] {client});
        Bank bank2 = ServiceFactory.getBankService().getBankByName("My Bank");

        assertTrue(TestService.isEquals(bank, bank2));
    }

    @Test
    public void saveExistingClientTest() throws Exception {
        Client client = new Client();
        client.setName("Oksana");
        client.setGender(Gender.FEMALE);
        SavingAccount account = new SavingAccount();
        account.setBalance(1000f);
        client.addAccount(account);
        ServiceFactory.getBankService().saveClient(bank, new Client[] {client});
        Bank bank2 = ServiceFactory.getBankService().getBankByName("My Bank");
        int numberOfClients1 = bank2.getClients().size();

        client.setCity("Odessa");
        ServiceFactory.getBankService().saveClient(bank, new Client[] {client});
        bank2 = ServiceFactory.getBankService().getBankByName("My Bank");
        int numberOfClients2 = bank2.getClients().size();

        assertTrue(TestService.isEquals(bank, bank2));
        assertTrue(numberOfClients1 == numberOfClients2);
    }

    @Test
    public void getClientTest() throws Exception {
        Client client = new Client();
        client.setName("Sergey");
        client.setGender(Gender.MALE);
        SavingAccount account = new SavingAccount();
        account.setBalance(1000f);
        client.addAccount(account);

        Client client1 = null;
        try {
            client1 = ServiceFactory.getBankService().getClient(bank, "Sergey");
        } catch (ClientNotFoundException e) {
        }
        assertTrue(client1 == null);
        ServiceFactory.getBankService().saveClient(bank, new Client[] {client});
        client1 = null;
        try {
            client1 = ServiceFactory.getBankService().getClient(bank, "Sergey");
        } catch (ClientNotFoundException e) {
        }
        assertTrue(TestService.isEquals(client1, client));
    }

    @Test
    public void removeClientTest() throws ServiceException, BankException {
        Client client = null;
        try {
            client = ServiceFactory.getBankService().getClient(bank, client1.getName());
        } catch (ClientNotFoundException e) {
        }
        assertTrue(client != null);
        ServiceFactory.getBankService().removeClient(bank, client);
        client = null;
        try {
            client = ServiceFactory.getBankService().getClient(bank, client1.getName());
        } catch (ClientNotFoundException e) {
        }
        assertTrue(client == null);
    }

}

