package com.luxoft.bankapp.Services;


import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.FeedException;
import com.luxoft.bankapp.Model.Bank;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class BankFeedService {
    private Bank bank;
    private Map<String, String> map;

    public BankFeedService(Bank bank) {
        this.bank = bank;
        map = new HashMap<String, String>();
    }

    public void loadFeeds(String folder) throws BankException {
        File f = new File(folder);
        if (!f.isDirectory()) {
            throw new FeedException("No such directory");
        }
        String[] files = f.list();
        for (int i = 0; i < files.length; i++) {
            if (files[i].endsWith(".feed")) {
                parseFeed(folder + "/" + files[i]);
            }
        }
    }

    public void parseFeed(String feed) throws BankException {
        try {
            FileReader fr = new FileReader(feed);
            BufferedReader in = new BufferedReader(fr);
            String str;
            while ((str = in.readLine()) != null) {
                if (str.trim().isEmpty()) {
                    continue;
                }
                String[] pairs = str.split(";");
                for (int i = 0; i < pairs.length; i++) {
                    String[] sa = pairs[i].split("=");
                    if (sa.length != 2) {
                        throw new FeedException("Only one \"=\" must be in each string");
                    }
                    map.put(sa[0].trim(), sa[1].trim());
                }
                bank.parseFeed(map);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}






