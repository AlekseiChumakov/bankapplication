import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class LoginServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        final String clientName = request.getParameter("name");
        if (clientName == null) {
            logger.warning("No client specified");
            throw new ServletException("No client specified");
        }
        Bank bank = (Bank)getServletContext().getAttribute("bank");
        if (bank == null) {
            response.sendRedirect("/welcome");
            return;
        }
        try {
            ServiceFactory.getBankService().getClient(bank, clientName);
        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        } catch (ClientNotFoundException e) {
            logger.warning("Client not found");
            ServletOutputStream out = response.getOutputStream();
            response.setContentType("text/html; charset=UTF-8");
            out.println("Client not found! <br>");
            out.println("<a href='login.html'>login</a>");
            return;
        }
        request.getSession().setAttribute("clientName", clientName);
        logger.info("Client " + clientName + " logged into ATM");
        response.sendRedirect("menu.html");
    }
}
