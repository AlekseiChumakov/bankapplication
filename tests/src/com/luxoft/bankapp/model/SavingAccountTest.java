package src.com.luxoft.bankapp.model;

import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.Model.SavingAccount;
import org.junit.Test;
import static org.junit.Assert.*;

public class SavingAccountTest {
    private SavingAccount sut;

    @Test
    public void testSetBalanceToZeroByDefault() {
        sut = new SavingAccount();

        assertEquals(0.0f, sut.getBalance(), 0.00001);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createSavingAccountWithNegativeBalance() {
        sut = new SavingAccount(-100.0f, 1);
    }

    @Test
    public void testDepositIncreaseBalance() {
        sut = new SavingAccount(100.0f, 1);
        sut.deposit(40.0f);

        assertEquals(140.0f, sut.getBalance(), 0.00001f);
    }

    @Test(expected = NotEnoughFundsException.class)
    public void testWithdrawMoreThanBalance() throws NotEnoughFundsException {
        sut = new SavingAccount(100.0f, 1);
        sut.withdraw(120.0f);
    }

    @Test
    public void testWithdrawLessThanBalance() throws NotEnoughFundsException {
        sut = new SavingAccount(100.0f, 1);
        sut.withdraw(20.0f);

        assertEquals(80.0f, sut.getBalance(), 0.00001f);
    }
}


