package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Services.ServiceFactory;

import java.util.List;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;

public class SetActiveAccountCommand extends AbstractCommand {
    public SetActiveAccountCommand() {
        super("Set active account");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException, BankException {
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined");
            return;
        }
        List<Account> accounts = ServiceFactory.getClientService().
                getAccounts(commander.getCurrentClient().getId(), commander.currentBank);
        if (accounts == null || accounts.size() == 0) {
            System.out.println("There are not accounts for a client");
            return;
        }
        System.out.println("Accessible accounts of client:");
        for (Account a: accounts) {
            System.out.println(a);
        }
        System.out.println("");

        int accountNumber;
        while (true) {
            try {
                accountNumber = Integer.parseInt(fromConsole("Enter the number of account which you want to do active: "));
                if (accountNumber < 0) {
                    System.out.println("Error! Number must be positive");
                    continue;
                }
                for (Account a: accounts) {
                    if (a.getId() == accountNumber) {
                        ServiceFactory.getClientService().setActiveAccount(commander.getCurrentClient(), a);
                        System.out.println("Active account succesfully changed");
                        return;
                    }
                }
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal account number");
                continue;
            }
        }

    }
}

