package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.util.List;
import java.util.Set;

public interface ClientDAO {
    Client findClientByName(Bank bank, String name) throws DAOException, ClientNotFoundException;
    Client findClientById(Bank bank, int clientId) throws DAOException, ClientNotFoundException;
    Set<Client> getAllClients(Bank bank) throws DAOException;
    void save(Bank bank, Client[] clients) throws DAOException;
    void remove(Bank bank, Client client) throws DAOException, ClientNotFoundException;
    List<Client> findClientsByCity(Bank bank, String city) throws DAOException;
}


