package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.AccountExistsException;
import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Model.*;

import java.sql.*;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class ClientDAOImpl implements ClientDAO {
    private final String queryFindClientByName =
            "SELECT ID, BANK_ID, NAME, GENDER, CITY, TELEPHONE, EMAIL, INITIAL_OVERDRAFT" +
                    " FROM CLIENTS WHERE BANK_ID = ? AND NAME = ?";
    private final String queryFindClientById =
            "SELECT ID, BANK_ID, NAME, GENDER, CITY, TELEPHONE, EMAIL, INITIAL_OVERDRAFT" +
                    " FROM CLIENTS WHERE BANK_ID = ? AND ID = ?";
    private final String queryGetAllClients =
            "SELECT ID, BANK_ID, NAME, GENDER, CITY, TELEPHONE, EMAIL, INITIAL_OVERDRAFT" +
                    " FROM CLIENTS WHERE BANK_ID = ?";
    private final String queryRemoveClient =
            "DELETE FROM CLIENTS WHERE ID = ? AND BANK_ID = ?";
    private final String queryInsertOrUpdate =
         "MERGE INTO CLIENTS(ID, BANK_ID, NAME, GENDER, CITY, TELEPHONE, EMAIL, INITIAL_OVERDRAFT) KEY(ID)" +
                 " VALUES(?, ?, ?, ?, ?, ?, ?, ?)";
    private final String queryGetIdByName =
            "SELECT ID FROM CLIENTS WHERE NAME = ?";
    private final String queryGetClientsByCity =
            "SELECT ID, BANK_ID, NAME, GENDER, CITY, TELEPHONE, EMAIL, INITIAL_OVERDRAFT" +
                    " FROM CLIENTS WHERE BANK_ID = ? AND CITY = ?";

    private static ClientDAOImpl instance;

    private ClientDAOImpl() {

    }

    public static ClientDAOImpl getInstance() {
        if (instance == null) {
            instance = new ClientDAOImpl();
        }
        return instance;
    }

    @Override
    public Client findClientByName(Bank bank, String name) throws DAOException, ClientNotFoundException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(queryFindClientByName);
            ps.setInt(1, bank.getId());
            ps.setString(2, name);
            rs = ps.executeQuery();
            Client client;
            if (rs.next()) {
                 client = createClientFromResultSet(rs);
            } else {
                throw new ClientNotFoundException();
            }
            rs.close();
            ps.close();
            DAOFactory.getBaseDAO().closeConnection();
            if (client != null){
                createClientAccounts(client, bank);
            }
            return client;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                    System.out.println("SQLException: " + e.getMessage());
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                    System.out.println("SQLException: " + e.getMessage());
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    @Override
    public Client findClientById(Bank bank, int clientId) throws DAOException, ClientNotFoundException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(queryFindClientById);
            ps.setInt(1, bank.getId());
            ps.setInt(2, clientId);
            rs = ps.executeQuery();
            Client client;
            if (rs.next()) {
                client = createClientFromResultSet(rs);
            } else {
                throw new ClientNotFoundException();
            }
            rs.close();
            ps.close();
            DAOFactory.getBaseDAO().closeConnection();
            if (client != null){
                createClientAccounts(client, bank);
            }
            return client;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    @Override
    public Set<Client> getAllClients(Bank bank) throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        Set<Client> clientsSet = null;
        try {
            ps = conn.prepareStatement(queryGetAllClients);
            ps.setInt(1, bank.getId());
            rs = ps.executeQuery();
            if (rs.next()) {
                clientsSet = new HashSet<Client>();
                do {
                    clientsSet.add(createClientFromResultSet(rs));
                } while (rs.next());
            }
            rs.close();
            ps.close();
            DAOFactory.getBaseDAO().closeConnection();
            if (clientsSet != null) {
                for (Client client: clientsSet) {
                    createClientAccounts(client, bank);
                }
            }
            return clientsSet;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    private Client createClientFromResultSet(ResultSet rs) throws SQLException {
            int clientID = rs.getInt("ID");
            int clientBankID = rs.getInt("BANK_ID");
            String clientName = rs.getString("NAME");
            String clientGender = rs.getString("GENDER");
            String clientCity = rs.getString("CITY");
            String clientTelephone = rs.getString("TELEPHONE");
            String clientEMAIL = rs.getString("EMAIL");
            float clientInitialOverdraft = rs.getFloat("INITIAL_OVERDRAFT");

            Client client = new Client();

            client.setId(clientID);
            client.setName(clientName);
            client.setCity(clientCity);
            client.setTelephone(clientTelephone);
            client.setEmail(clientEMAIL);
            client.setGender(clientGender.matches("[Mm]") ? Gender.MALE : (clientGender.matches("[Ff]") ? Gender.FEMALE : null));
            client.setInitialOverdraft(clientInitialOverdraft);
            return client;
    }

    private Client createClientAccounts(Client client, Bank bank) throws DAOException {
        if (client != null) {
            List<Account> accounts = DAOFactory.getAccountDAO().getClientAccounts(client.getId(), bank);
            if (accounts == null) {
                return client;
            }
            Account activeAccount = DAOFactory.getAccountDAO().getActiveAccount(client.getName());
            for (Account account: accounts) {
                try {
                    client.addAccount(account);
                    if (activeAccount != null && account.getId() == activeAccount.getId()) {
                        client.setActiveAccount(account);
                    }
                } catch (AccountExistsException e) {}
            }
        }
        return client;
    }

    @Override
    public void remove(Bank bank, Client client) throws DAOException, ClientNotFoundException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        try {
            ps = conn.prepareStatement(queryRemoveClient);
            ps.setInt(1, client.getId());
            ps.setInt(2, bank.getId());
            if (ps.executeUpdate() == 0) {
                throw new DAOException("Impossible to update Account in DB. Transaction is rolled back");
            }
            ps.close();
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    @Override
    public void save(Bank bank, Client[] clients) throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(queryInsertOrUpdate);
            for (Client client: clients) {
                if (client.getId() == 0) {
                    ps.setNull(1, Types.NULL);
                } else {
                    ps.setInt(1, client.getId());
                }
                ps.setInt(2, bank.getId());
                if (client.getName() == null) {
                    throw new DAOException("Impossible to update Client in DB. Client name undefined");
                }
                ps.setString(3, client.getName());
                if (client.getGender() == null) {
                    throw new DAOException("Impossible to update Client in DB. Client gender undefined");
                }
                ps.setString(4, client.getGender().getGenderChar());
                if (client.getCity() == null) {
                    ps.setNull(5, Types.NULL);
                } else {
                    ps.setString(5, client.getCity());
                }
                if (client.getTelephone() == null) {
                    ps.setNull(6, Types.NULL);
                } else {
                    ps.setString(6, client.getTelephone());
                }
                if (client.getEmail() == null) {
                    ps.setNull(7, Types.NULL);
                } else {
                    ps.setString(7, client.getEmail());
                }
                ps.setFloat(8, client.getInitialOverdraft());
                ps.addBatch();
            }
            final int[] results = ps.executeBatch();
            ps.close();
            for (int i = 0; i < results.length; i++) {
                if (results[i] == Statement.EXECUTE_FAILED || results[i] == Statement.SUCCESS_NO_INFO) {
                    throw new DAOException("Impossible to update Client in DB. Transaction is rolled back");
                }
            }
            ps = conn.prepareStatement(queryGetIdByName);
            for(Client client: clients) {
                ps.setString(1, client.getName());
                rs = ps.executeQuery();
                if (!rs.next()) {
                    throw new DAOException("Impossible to save in DB. Can't get clientID");
                }
                client.setId(rs.getInt("ID"));
                rs.close();
            }
            for(Client client: clients) {
                DAOFactory.getAccountDAO().save(client.getAccounts().toArray(new Account[0]), client);
                DAOFactory.getAccountDAO().setActiveAccount(client, client.getActiveAccount());
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    @Override
    public List<Client> findClientsByCity(Bank bank, String city) throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        List<Client> clientsList = null;
        try {
            ps = conn.prepareStatement(queryGetClientsByCity);
            ps.setInt(1, bank.getId());
            ps.setString(2, city);
            rs = ps.executeQuery();
            if (rs.next()) {
                clientsList = new ArrayList<Client>();
                do {
                    clientsList.add(createClientFromResultSet(rs));
                } while (rs.next());
            }
            rs.close();
            ps.close();
            DAOFactory.getBaseDAO().closeConnection();
            if (clientsList != null) {
                for (Client client: clientsList) {
                    createClientAccounts(client, bank);
                }
            }
            return clientsList;
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException e) {
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }
}


