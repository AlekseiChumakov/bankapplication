package com.luxoft.bankapp.ClientServer;

import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Model.Gender;
import com.luxoft.bankapp.Services.BankService;
import com.luxoft.bankapp.Services.ServiceFactory;
import sun.misc.BASE64Encoder;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.util.Date;
import java.util.logging.*;

import static com.luxoft.bankapp.ClientServer.BankServer.*;

public class ServerThread implements Runnable {
    private Socket connection;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Bank currentBank;
    public final static Logger logger = Logger.getLogger("com.luxoft.bankapp.bankserver");

    public ServerThread(Socket clientSocket, Bank bank) {
        connection = clientSocket;
        currentBank = bank;
    }

    @Override
    public void run() {
        BankServerThreaded.getInstance().waitingClientsNumber.decrementAndGet();
        Date startDate = new Date();
        if (logger.isLoggable(Level.INFO)) {
            logger.info("client service has been started at " + startDate);
        }
        try {
            out = new ObjectOutputStream(connection.getOutputStream());
            out.flush();
            out.writeObject(readyPrefix);
            out.flush();
            in = new ObjectInputStream(connection.getInputStream());
            String message;
            while (true) {
                try {
                    message = (String) in.readObject();
                    String[] splitMessage = message.split(separator);
                    if (splitMessage[0].equals(findClientPrefix)) {
                        try {
                            if (logger.isLoggable(Level.FINE)) {
                                logger.fine("trying to get client from database");
                            }
                            message = okPrefix + ServiceFactory.getBankService().getClient(currentBank, splitMessage[1]).getName();
                        } catch (ClientNotFoundException e) {
                            logger.log(Level.SEVERE, "client not found with name " + splitMessage[1], e);
                            message = errorPrefix + "Client not found";
                        }
                        out.writeObject(message);
                        out.flush();
                    } else if (splitMessage[0].equals(getBalancePrefix)) {
                        if (logger.isLoggable(Level.FINE)) {
                            logger.fine("trying to get active account from database");
                        }
                        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(splitMessage[1]);
                        if (activeAccount == null) {
                            if (logger.isLoggable(Level.WARNING)) {
                                logger.warning("no active account for " + splitMessage[0]);
                            }
                            message = errorPrefix + "No active account for this client";
                            out.writeObject(message);
                            out.flush();
                            continue;
                        }
                        message = okPrefix + activeAccount;
                        out.writeObject(message);
                        out.flush();
                    } else if (splitMessage[0].equals(withdrawPrefix)) {
                        synchronized (ServerThread.class) {
                            if (logger.isLoggable(Level.FINE)) {
                                logger.fine("trying to get active account from database");
                            }
                            Account activeAccount = ServiceFactory.getClientService().getActiveAccount(splitMessage[1]);
                            if (activeAccount == null) {
                                if (logger.isLoggable(Level.WARNING)) {
                                    logger.warning("no active account for " + splitMessage[0]);
                                }
                                message = errorPrefix + "No active account for this client";
                                out.writeObject(message);
                                out.flush();
                                continue;
                            }
                            float amount = Float.parseFloat(splitMessage[2]);
                            try {
                                if (logger.isLoggable(Level.FINE)) {
                                    logger.fine("trying to make withdraw operation in database");
                                }
                                ServiceFactory.getAccountService().withdraw(activeAccount, amount);
                                message = okPrefix + "Successfully! " + activeAccount;
                            } catch (OverDraftLimitExceededException e) {
                                logger.log(Level.SEVERE, e.getMessage(), e);
                                message = errorPrefix + "Overdraft limit exceeded";
                            } catch (NotEnoughFundsException e) {
                                logger.log(Level.SEVERE, "no enough funds", e);
                                        message = errorPrefix + "No enough funds";
                            } catch (BankException e) {
                                logger.log(Level.SEVERE, "bank error", e);
                                message = errorPrefix + e.getMessage();
                            } finally {
                                out.writeObject(message);
                                out.flush();
                                continue;
                            }
                        }
                    } else if (splitMessage[0].equals(exitPrefix)) {
                        message = okPrefix + "Bye!";
                        out.writeObject(message);
                        out.flush();
                        break;
                    } else if (splitMessage[0].equals(bankInfoPrefix)) {
                        if (logger.isLoggable(Level.FINE)) {
                            logger.fine("trying to get bank info from database");
                        }
                        message = okPrefix + ServiceFactory.getBankService().getBankInfo(currentBank);
                        out.writeObject(message);
                        out.flush();
                        continue;
                    } else if (splitMessage[0].equals(clientInfoPrefix)) {
                        try {
                            if (logger.isLoggable(Level.FINE)) {
                                logger.fine("trying to get client from database");
                            }
                            Client client = ServiceFactory.getBankService().getClient(currentBank, splitMessage[1]);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(baos);
                            oos.writeObject(client);
                            baos.flush();
                            StringBuilder sb = new StringBuilder(new BASE64Encoder().encode(baos.toByteArray()));
                            oos.close();
                            baos.close();
                            sb.insert(0, okPrefix);
                            out.writeObject(sb.toString());
                            out.flush();
                        } catch (ClientNotFoundException e) {
                            logger.log(Level.SEVERE, "client not found with name " + splitMessage[1], e);
                            message = errorPrefix + "Client not found";
                            out.writeObject(message);
                            out.flush();
                        }
                        continue;
                    } else if (splitMessage[0].equals(addClientPrefix)) {
                        Client client = new Client();
                        client.setName(splitMessage[1]);
                        client.setGender(splitMessage[2].matches("[Mm]") ? Gender.MALE : (splitMessage[2].matches("[Ff]") ? Gender.FEMALE : null));
                        client.setCity(splitMessage[3]);
                        client.setTelephone(splitMessage[4]);
                        client.setEmail(splitMessage[5]);
                        client.setInitialOverdraft(Float.parseFloat(splitMessage[6]));
                        try {
                            logger.fine("trying to get client from database");
                            ServiceFactory.getBankService().getClient(currentBank, client.getName());
                            if (logger.isLoggable(Level.FINE)) {
                                logger.fine("trying to save client in database");
                            }
                            ServiceFactory.getBankService().saveClient(currentBank, new Client[]{client});
                            message = okPrefix + "Client was added successfully";
                        } catch (BankException e) {
                            logger.log(Level.SEVERE, "bank error", e);
                            message = errorPrefix + e.getMessage();
                        }
                        out.writeObject(message);
                        out.flush();
                        continue;
                    } else if (splitMessage[0].equals(removeClientPrefix)) {
                        BankService bs = ServiceFactory.getBankService();
                        Client client;
                        try {
                            client = bs.getClient(currentBank, splitMessage[1]);
                            if (logger.isLoggable(Level.FINE)) {
                                logger.fine("trying to remove client from database");
                            }
                            bs.removeClient(currentBank, client);
                            message = okPrefix + client.getClientSalutation() + " successfully removed";
                        } catch (ClientNotFoundException e) {
                            logger.log(Level.SEVERE, "client not found with name " + splitMessage[1], e);
                            message = errorPrefix + "Client not found";
                        }
                        out.writeObject(message);
                        out.flush();
                        continue;
                    }
                } catch (ClassNotFoundException classnot) {
                    logger.log(Level.SEVERE, "data received in unknown format", classnot);
                } catch (ServiceException e) {
                    logger.log(Level.SEVERE, "service error", e);
                    System.exit(0);
                }
            }
        } catch (IOException ioException) {
            logger.log(Level.SEVERE, "I/O error", ioException);
        } finally {
            try {
                if (in != null) {
                    in.close();
                }
                if (out != null) {
                    out.close();
                }
                if (connection != null) {
                    connection.close();
                }
            } catch (IOException ioException) {
                logger.log(Level.SEVERE, "I/O error", ioException);
            }
        }
        if (logger.isLoggable(Level.INFO)) {
            logger.info("client service has been finished, service time: " +
                    (double) (new Date().getTime() - startDate.getTime()) / 1000 + " s");
        }
    }
}

