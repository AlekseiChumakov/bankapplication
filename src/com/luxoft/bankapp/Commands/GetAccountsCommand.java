package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Services.ServiceFactory;

import java.util.List;

public class GetAccountsCommand extends AbstractCommand {
    public GetAccountsCommand() {
        super("Get accounts");
    }

    @Override
    public void execute(BankCommander commander) throws BankException, ServiceException {
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined\n");
            return;
        }
        List<Account> accountsList = ServiceFactory.getClientService().
                getAccounts(commander.getCurrentClient().getId(), commander.currentBank);
        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(commander.getCurrentClient().getName());

        if (accountsList == null || accountsList.size() == 0) {
            System.out.println("Current client doesn't have any accounts\n");
            return;
        }
        for (Account a: accountsList) {
            System.out.print(a);
            if (activeAccount != null && (activeAccount.getId() == a.getId())) {
                System.out.print("  = ACTIVE =");
            }
            System.out.println("");
        }
    }
}








