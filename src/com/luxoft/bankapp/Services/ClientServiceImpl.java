package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.DAO.DAOFactory;
import com.luxoft.bankapp.Exceptions.AccountExistsException;
import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;


import java.util.List;

public class ClientServiceImpl implements ClientService {
    private static ClientServiceImpl instance;

    private ClientServiceImpl() {

    }

    public static ClientServiceImpl getInstance() {
        if (instance == null) {
            instance = new ClientServiceImpl();
        }
        return instance;
    }

    @Override
    public void saveAccount(Client client, Account[] accounts) throws ServiceException, AccountExistsException {
        try {
            DAOFactory.getAccountDAO().save(accounts, client);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void setActiveAccount(Client client, Account account) throws ServiceException {
        try {
            DAOFactory.getAccountDAO().setActiveAccount(client, account);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Account> getAccounts(int clientId, Bank bank) throws ServiceException {
        try {
            return DAOFactory.getAccountDAO().getClientAccounts(clientId, bank);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Account getActiveAccount(String clientName) throws ServiceException {
        try {
            return DAOFactory.getAccountDAO().getActiveAccount(clientName);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }
}

