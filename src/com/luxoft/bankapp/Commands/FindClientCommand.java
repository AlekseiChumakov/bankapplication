package com.luxoft.bankapp.Commands;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;
import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.ServiceFactory;

public class FindClientCommand extends AbstractCommand {
    public FindClientCommand() {
        super("Find client");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException, BankException {
        if (BankCommander.currentBank == null) {
            System.out.println("Error! Current bank is undefined\n");
            return;
        }
        String clientName = fromConsole("Input client name: \n");
        Client client = ServiceFactory.getBankService().getClient(BankCommander.currentBank, clientName);
        commander.setCurrentClient(client);
        System.out.println("Client is selected: " + client);
    }
}








