package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Services.ServiceFactory;

public class ReportCommand extends AbstractCommand {
    public ReportCommand() {
        super("Bank info");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException {
        System.out.println(ServiceFactory.getBankService().getBankInfo(commander.currentBank));
    }
}

