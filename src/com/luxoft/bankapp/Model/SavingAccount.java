package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import java.util.Map;

public class SavingAccount extends AbstractAccount {
    public SavingAccount() {
    }

    public SavingAccount(int id) {
        super(id);
    }

    public SavingAccount(float initialBalance, int id) {
        super(initialBalance, id);
    }

    @Override
    public void deposit(float x) {
        setBalance(getBalance() + x);
    }

    @Override
    public void withdraw(float x) throws NotEnoughFundsException {
        if (x > getBalance()) {
            throw new NotEnoughFundsException();
        }
        setBalance(getBalance() - x);
    }

    @Override
    public void printReport() {
        System.out.println("Счет №" + getId() + ": SavingAccount; balance: " + getBalance());
    }

    @Override
    public void parseFeed(Map<String, String> feed) {
        super.parseFeed(feed);
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof SavingAccount)) {
            return false;
        }
        SavingAccount ca = (SavingAccount) obj;
        return getId() == ca.getId();

    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("SavingAccount №");
        str.append(getId()).append("; Balance: ").append(getBalance()).append(" (Round = ")
                .append(decimalValue()).append(")");
        return str.toString();
    }
}








