package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.util.List;
import java.util.Map;
import java.util.Set;

public class BankInfo {
    private String bankName;
    private int numberOfClients;
    private int accountsNumber;
    private double bankCreditSum;
    private Map<String, List<Client>> clientsByCity;
    private Set<Client> clientsSorted;

    public BankInfo() {

    }

    public void initialize(Bank bank) {

    }

    public int getNumberOfClients() {
        return numberOfClients;
    }

    public int getAccountsNumber() {
        return accountsNumber;
    }

    public double getBankCreditSum() {
        return bankCreditSum;
    }

    public Map<String, List<Client>> getClientsByCity() {
        return clientsByCity;
    }

    public Set<Client> getClientsSorted() {
        return clientsSorted;
    }

    public String getBankName() {
        return bankName;
    }

    public void setBankName(String name) {
        bankName = name;
    }

    public void setNumberOfClients(int num) {
        numberOfClients = num;
    }

    public void setAccountsNumber(int num) {
        accountsNumber = num;
    }

    public void setBankCreditSum(double sum) {
        bankCreditSum = sum;
    }

    public void  setClientsByCity(Map<String, List<Client>> map) {
        clientsByCity = map;
    }

    public void setClientsSorted(Set<Client> set) {
        clientsSorted = set;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("There are ").append(numberOfClients).append(" client(s) registered in bank \"")
                .append(bankName).append("\"\nGeneral number of accounts ").append(accountsNumber);
        sb.append("\nGeneral credit value ").append(bankCreditSum);
        sb.append("\n\nClients summary balance:\n");
        for (Client c: clientsSorted) {
            sb.append(c.getClientSalutation()).append(" = ").append(c.getSummaryBalance()).append("\n");
        }
        sb.append("\nClients by cities:\n");
        for (String s : clientsByCity.keySet()) {
            String result = "City " + s + ": ";
            sb.append("\n");
            sb.append(result);
            sb.append("\n");
            for (Client c : clientsByCity.get(s)) {
                result = c.getClientSalutation();
                sb.append(result);
                sb.append("\n");
            }
        }
        return sb.toString();
    }
}



