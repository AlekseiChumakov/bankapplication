package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Exceptions.FeedException;
import com.luxoft.bankapp.Exceptions.OverDraftLimitExceededException;
import java.util.Map;

public class CheckingAccount extends AbstractAccount {
    private float overdraftCurrentValue;
    private float overdraftStartValue;

    public CheckingAccount() {
    }

    public CheckingAccount(float initialBalance, float initialOverdraft, int accountNumber) {
        super(initialBalance, accountNumber);
        if (initialOverdraft < 0) {
            throw new IllegalArgumentException();
        }
        overdraftStartValue = overdraftCurrentValue = initialOverdraft;
    }

    public CheckingAccount(float initialBalance, float initialOverdraft, float currentOverdraft, int accountNumber) {
        super(initialBalance, accountNumber);
        if (initialOverdraft < 0 || currentOverdraft < 0 || currentOverdraft > initialOverdraft) {
            throw new IllegalArgumentException();
        }
        overdraftStartValue = initialOverdraft;
        overdraftCurrentValue = currentOverdraft;
    }

    public float getCredit() {
            return overdraftStartValue - overdraftCurrentValue;
    }

    @Override
    public void deposit(float x) {
        if (overdraftCurrentValue < overdraftStartValue) {
            float temp = overdraftStartValue - overdraftCurrentValue;
            if (x > temp) {
                setBalance(getBalance() + x - temp);
                overdraftCurrentValue = overdraftStartValue;
            } else overdraftCurrentValue += x;
        } else {
            setBalance(getBalance() + x);
        }
    }

    @Override
    public void withdraw(float x) throws OverDraftLimitExceededException {
        if (x > getBalance() + overdraftCurrentValue) {
            throw new OverDraftLimitExceededException(this, getBalance() + overdraftCurrentValue);
        }
        if (x > getBalance()) {
            overdraftCurrentValue -= x - getBalance();
            setBalance(0);

        } else {
            setBalance(getBalance() - x);
        }
    }

    @Override
    public void printReport() {
        System.out.println("Счет №" + getId() + ": CheckingAccount; balance: " + getBalance() +
                "; доступный овердрафт:  " + overdraftCurrentValue);
    }

    public void setOverdraftStartValue(float f) {
        if (f < 0) {
            throw new IllegalArgumentException();
        }
        overdraftStartValue = overdraftCurrentValue = f;
    }

    public void setOverdraftCurrentValue(float f) {
        if (overdraftStartValue < f || f < 0) {
            throw new IllegalArgumentException();
        }
        overdraftCurrentValue = f;
    }

    public float getOverdraftCurrentValue() {
        return overdraftCurrentValue;
    }

    public float getOverdraftStartValue() {
        return overdraftStartValue;
    }

    @Override
    public void parseFeed(Map<String, String> feed) {
        super.parseFeed(feed);
        try {
            setOverdraftStartValue(Float.valueOf(feed.get("overdraft")));
        } catch (NullPointerException e) {
            throw new FeedException("Error reading overdraft");
        } catch (NumberFormatException e) {
            throw new FeedException("Error reading overdraft");
        }
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof CheckingAccount)) {
            return false;
        }
        CheckingAccount ca = (CheckingAccount) obj;
        return getId() == ca.getId();
    }

    @Override
    public int hashCode() {
        return getId();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("CheckingAccount №");
        str.append(getId()).append("; Balance: ").append(getBalance()).append(" (Round = ").append(decimalValue()).append("); ")
                .append("Overdraft: ").append(overdraftCurrentValue).append("(").append(overdraftStartValue).append(")");
        return str.toString();
    }

    @Override
    public void setBalance(float f) {
        super.setBalance(f);
    }
}








