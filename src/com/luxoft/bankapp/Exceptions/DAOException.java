package com.luxoft.bankapp.Exceptions;

public class DAOException extends Exception {
    public DAOException(String str) {
        super(str);
    }
}

