import com.luxoft.bankapp.DAO.BankDAO;
import com.luxoft.bankapp.DAO.DAOFactory;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.test.service.TestService;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class BankDaoTest {
    private Bank bank1;

    @Before
    public void initBank() throws Exception {
        DAOFactory.getBankDAO().clearBank();
        bank1 = new Bank ();
        bank1.setName("My Bank");
        Client client = new Client();
        client.setName("Ivan");
        client.setGender(Gender.MALE);
        client.setCity("Kiev");
        client.addAccount(new CheckingAccount(100f, 100f, 1)); //  CheckingAccount(balance, overdraft, id)
        client.addAccount(new CheckingAccount(200f, 200f, 2));
        client.addAccount(new SavingAccount(150f, 3));         //  SavingAccount(balance, id)
        client.addAccount(new SavingAccount(250f, 4));

        Client client2 = new Client();
        client2.setName("Viktor");
        client2.setGender(Gender.MALE);
        client2.setCity("Kiev");
        client2.addAccount(new CheckingAccount(1000f, 700f, 5)); //  CheckingAccount(balance, overdraft, id)
        client2.addAccount(new SavingAccount(1200f, 6));
        client2.addAccount(new SavingAccount(350f, 7));         //  SavingAccount(balance, id)
        client2.addAccount(new SavingAccount(750f, 8));

        Client client3= new Client();
        client3.setName("Maria");
        client3.setGender(Gender.FEMALE);
        client3.setCity("Odessa");
        client3.addAccount(new SavingAccount(5000, 9));
        client3.addAccount(new CheckingAccount(2000f, 1000f, 10));

        bank1.addClient(client);
        bank1.addClient(client2);
        bank1.addClient(client3);
    }

    @Test
    public void testInsert() throws Exception {
        DAOFactory.getBankDAO().save(bank1);
        Bank bank2 = DAOFactory.getBankDAO().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank1, bank2));
    }

    @Test
    public void testUpdate() throws Exception {
        BankDAO bankDao = DAOFactory.getBankDAO();
        bankDao.save(bank1);
        Client client2 = new Client();
        client2.setName("Mark");
        client2.setGender(Gender.MALE);
        client2.setCity("New York");
        client2.addAccount(new SavingAccount(4000, 12));
        Account account = new SavingAccount(3000, 13);
        client2.addAccount(account);
        bank1.addClient(client2);
        bankDao.save(bank1);
        Bank bank2 = DAOFactory.getBankDAO().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank1, bank2));

        client2.setCity("Herson");
        bankDao.save(bank1);
        assertFalse(TestService.isEquals(bank1, bank2));
        bank2 = DAOFactory.getBankDAO().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank1, bank2));

        client2.getAccounts().iterator().next().deposit(300f);
        bankDao.save(bank1);
        assertFalse(TestService.isEquals(bank1, bank2));
        bank2 = DAOFactory.getBankDAO().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank1, bank2));

        client2.setActiveAccount(account);
        bankDao.save(bank1);
        assertFalse(TestService.isEquals(bank1, bank2));
        bank2 = DAOFactory.getBankDAO().getBankByName("My Bank");
        assertTrue(TestService.isEquals(bank1, bank2));
    }
}

