package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.DAO.*;
import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.io.*;
import java.security.Provider;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class BankServiceImpl implements BankService {
    private final String serializationFile = "client.obj";
    private static BankServiceImpl instance;

    private BankServiceImpl() {

    }

    public static BankServiceImpl getInstance() {
        if (instance == null) {
            instance = new BankServiceImpl();
        }
        return instance;
    }

    @Override
    public Bank getBankByName(String name) throws ServiceException {
        try{
            return DAOFactory.getBankDAO().getBankByName(name);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void saveClient(Bank bank, Client[] clients) throws ServiceException {
        try {
           DAOFactory.getClientDAO().save(bank, clients);
            for (Client client: clients) {
                try {
                    bank.addClient(client);
                } catch (ClientExistsException e) {
                    bank.removeClient(client);
                    try {
                        bank.addClient(client);
                    } catch (ClientExistsException ex) {
                        throw new ServiceException("add client error");
                    }
                }
            }
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void removeClient(Bank bank, Client client) throws ServiceException, ClientNotFoundException {
        try {
            DAOFactory.getClientDAO().remove(bank, client);
            bank.removeClient(client);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Client getClient(Bank bank, String clientName) throws ServiceException, ClientNotFoundException {
        try {
            return DAOFactory.getClientDAO().findClientByName(bank, clientName);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Client getClient(Bank bank, int id) throws ServiceException, ClientNotFoundException {
        try {
            return DAOFactory.getClientDAO().findClientById(bank, id);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void saveClientToFile(Client c) {
        OutputStream fos = null;
        ObjectOutputStream oos = null;
        try {
            fos = new FileOutputStream(serializationFile);
            oos = new ObjectOutputStream(fos);
            oos.writeObject(c);
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (oos != null) {
                try {
                    oos.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fos != null) {
                try {

                    fos.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    @Override
    public Client loadClientFromFile() {
        Client c = null;
        InputStream fis = null;
        ObjectInputStream ois = null;
        try {
            fis = new FileInputStream(serializationFile);
            ois = new ObjectInputStream(fis);
            c = (Client) ois.readObject();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ClassNotFoundException e) {
        } finally {
            if (ois != null) {
                try {
                    ois.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            if (fis != null) {
                try {

                    fis.close();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
        return c;
    }

    @Override
    public BankInfo getBankInfo(Bank bank) throws ServiceException {
        try {
            return DAOFactory.getBankDAO().getBankInfo(bank);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void clearBank() throws ServiceException {
        try {
            DAOFactory.getBankDAO().clearBank();
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public void save(Bank bank) throws ServiceException {
        try {
            DAOFactory.getBankDAO().save(bank);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public List<Client> findClientsByCity(Bank bank, String city) throws ServiceException {
        try {
            return DAOFactory.getClientDAO().findClientsByCity(bank, city);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    public Set<Client> getAllClients(Bank bank) throws ServiceException {
        try {
            return DAOFactory.getClientDAO().getAllClients(bank);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }
}








