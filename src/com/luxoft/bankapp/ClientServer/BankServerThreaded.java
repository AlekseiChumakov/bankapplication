package com.luxoft.bankapp.ClientServer;

import com.luxoft.bankapp.Model.Bank;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.logging.*;

public class BankServerThreaded implements Runnable {
    private static BankServerThreaded instance;
    private final int POOL_SIZE = 10;
    private final int PORT = 2004;
    private ServerSocket serverSocket;
    private Bank currentBank;
    public AtomicInteger waitingClientsNumber = new AtomicInteger(0);
    public final static Logger logger = Logger.getLogger("com.luxoft.bankapp.bankserver");

    private BankServerThreaded() {
        Handler handler = null;
        try {
            handler = new FileHandler("server.log", true);
        } catch (IOException e) {
            System.out.println("Error creating logger");
            System.exit(0);
        }
        handler.setFormatter(new SimpleFormatter());
        logger.addHandler(handler);
        logger.setLevel(Level.ALL);
    }

    public static BankServerThreaded getInstance() {
        if (instance == null) {
            instance = new BankServerThreaded();
        }
        return instance;
    }

    public void init (Bank bank) {
        currentBank = bank;
    }

    @Override
    public void run() {
        try {
            serverSocket = new ServerSocket(PORT);
            ExecutorService pool = Executors.newFixedThreadPool(POOL_SIZE);
            Thread bsMonitor = new Thread(new BankServerMonitor());
            bsMonitor.setDaemon(true);
            bsMonitor.start();
            while (true) {
                Socket clientSocket = serverSocket.accept();
                if (logger.isLoggable(Level.FINER)) {
                    logger.finer("new client socket was created");
                }
                waitingClientsNumber.incrementAndGet();
                pool.submit(new ServerThread(clientSocket, currentBank));
            }
        } catch (IOException e) {
            logger.log(Level.SEVERE, "server socket error", e);
        }
    }
}

