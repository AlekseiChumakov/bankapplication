import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class GetClientServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String param = request.getParameter("id");
        if (param == null) {
            logger.warning("id not specified");
            throw new ServletException("id not specified");
        }
        int id;
        try {
            id = Integer.parseInt(param);
        } catch (Exception e) {
            logger.log(Level.SEVERE, "can't convert id to int", e);
            throw new ServletException(e.getMessage());
        }
        Bank bank = (Bank)getServletContext().getAttribute("bank");
        if (bank == null) {
            logger.warning("No bank specified");
            throw new ServletException("No bank specified");
        }
        Client client;
        try {
            client = ServiceFactory.getBankService().getClient(bank, id);
        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        } catch (ClientNotFoundException e) {
            logger.warning("Client not found");
            throw new ServletException("Client not found");
        }
        request.setAttribute("client", client);
        request.getRequestDispatcher("/client.jsp").forward(request, response);
    }
}
