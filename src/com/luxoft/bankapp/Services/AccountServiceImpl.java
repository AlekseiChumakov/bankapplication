package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.DAO.DAOFactory;
import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Account;

public class AccountServiceImpl implements AccountService {
    private static AccountServiceImpl instance;

    private AccountServiceImpl() {

    }

    public static AccountServiceImpl getInstance() {
        if (instance == null) {
            instance = new AccountServiceImpl();
        }
        return instance;
    }

    @Override
    synchronized public void deposit(Account account, float amount) throws ServiceException {
        account.deposit(amount);
        try {
            DAOFactory.getAccountDAO().save(new Account[]{account}, null);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }

    }

    @Override
    synchronized public void withdraw(Account account, float amount) throws ServiceException, NotEnoughFundsException {
        try {
            account.withdraw(amount);
            DAOFactory.getAccountDAO().save(new Account[]{account}, null);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }

    @Override
    synchronized public void transfer(Account account1, Account account2, float amount) throws ServiceException, NotEnoughFundsException {
        account1.withdraw(amount);
        account2.deposit(amount);
        try {
            DAOFactory.getAccountDAO().save(new Account[]{account1, account2}, null);
        } catch (DAOException e) {
            throw new ServiceException(e.getMessage());
        }
    }
}

