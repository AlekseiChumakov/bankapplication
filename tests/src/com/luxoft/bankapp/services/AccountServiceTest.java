package src.com.luxoft.bankapp.services;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.Exceptions.OverDraftLimitExceededException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;
import org.junit.Before;
import org.junit.Test;
import static org.junit.Assert.*;

public class AccountServiceTest {
    Bank bank;
    Client client1;
    Client client2;
    CheckingAccount account1;
    SavingAccount account2;

    @Before
    public void initBank() throws ServiceException, BankException {
        ServiceFactory.getBankService().clearBank();
        bank = new Bank("My Bank");
        client1 = new Client();
        client1.setName("Mark");
        client1.setGender(Gender.MALE);
        client2 = new Client();
        client2.setName("Maria");
        client2.setGender(Gender.FEMALE);
        account1 = new CheckingAccount();
        client1.addAccount(account1);
        account2 = new SavingAccount();
        client2.addAccount(account2);
        bank.addClient(client1);
        bank.addClient(client2);
    }

    @Test
    public void testDeposit() throws ServiceException {
        account1.setBalance(0);
        account1.setOverdraftStartValue(1000f);
        account1.setOverdraftCurrentValue(700f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().deposit(account1, 500f);
        assertTrue(account1.getBalance() == 200f);
        assertTrue(account1.getOverdraftCurrentValue() == 1000f);

        ServiceFactory.getAccountService().deposit(account2, 500f);
        assertTrue(account2.getBalance() == 1500f);
    }

    @Test
    public void testWithdraw() throws ServiceException, BankException {
        account1.setBalance(1000f);
        account1.setOverdraftStartValue(500f);
        account1.setOverdraftCurrentValue(500f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().withdraw(account1, 800f);
        assertTrue(account1.getBalance() == 200f);
        assertTrue(account1.getOverdraftStartValue() == 500f);
        assertTrue(account1.getOverdraftCurrentValue() == 500f);

        account1.setBalance(1000f);
        account1.setOverdraftStartValue(500f);
        account1.setOverdraftCurrentValue(500f);

        ServiceFactory.getAccountService().withdraw(account1, 1200f);
        assertTrue(account1.getBalance() == 0);
        assertTrue(account1.getOverdraftStartValue() == 500f);
        assertTrue(account1.getOverdraftCurrentValue() == 300f);

        ServiceFactory.getAccountService().withdraw(account2, 300f);
        assertTrue(account2.getBalance() == 700);
    }

    @Test(expected = OverDraftLimitExceededException.class)
    public void testCheckingAccountWithdrawExceptions() throws ServiceException, NotEnoughFundsException {
        account1.setBalance(1000f);
        account1.setOverdraftStartValue(500f);
        account1.setOverdraftCurrentValue(500f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().withdraw(account1, 1501f);
    }

    @Test(expected = NotEnoughFundsException.class)
    public void testSavingAccountWithdrawExceptions() throws ServiceException, NotEnoughFundsException {
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().withdraw(account1, 1001f);
    }
    @Test
    public void testTransfer() throws ServiceException, NotEnoughFundsException {
        account1.setBalance(500);
        account1.setOverdraftStartValue(1000f);
        account1.setOverdraftCurrentValue(800f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().transfer(account1, account2, 1200f);
        assertTrue(account1.getBalance() == 0);
        assertTrue(account1.getOverdraftCurrentValue() == 100);
        assertTrue(account2.getBalance() == 2200);
    }

    @Test(expected = OverDraftLimitExceededException.class)
    public void testTransferExceptions() throws ServiceException, NotEnoughFundsException {
        account1.setBalance(500);
        account1.setOverdraftStartValue(1000f);
        account1.setOverdraftCurrentValue(800f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);

        ServiceFactory.getAccountService().transfer(account1, account2, 1301f);
    }
}

