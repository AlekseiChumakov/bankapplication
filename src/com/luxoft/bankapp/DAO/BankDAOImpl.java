package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.CheckingAccount;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.BankInfo;

import java.sql.*;
import java.util.*;

public class BankDAOImpl implements BankDAO {
    private final String queryFindByName =
            "SELECT ID, NAME FROM BANKS WHERE NAME = ?";
    private final String queryNumberOfClients =
            "SELECT COUNT(CLIENTS.*) FROM CLIENTS JOIN BANKS ON CLIENTS.BANK_ID = BANKS.ID" +
            " WHERE BANKS.NAME = ?";
    private final String queryNumberOfAccounts =
            "SELECT COUNT(ACCOUNTS.*) FROM ACCOUNTS JOIN CLIENTS ON ACCOUNTS.CLIENT_ID = CLIENTS.ID" +
            " JOIN BANKS ON CLIENTS.BANK_ID = BANKS.ID WHERE BANKS.NAME = ?";
    private final String queryBankCreditSum =
            "SELECT SUM(ACCOUNTS.OVERDRAFT - ACCOUNTS.CURRENT_OVERDRAFT) FROM ACCOUNTS JOIN CLIENTS" +
            " ON ACCOUNTS.CLIENT_ID = CLIENTS.ID JOIN BANKS ON CLIENTS.BANK_ID = BANKS.ID" +
            " WHERE ACCOUNTS.OVERDRAFT IS NOT NULL AND BANKS.NAME = ?";
    private final String queryGetClientsSortedBySummaryBalance =
            "SELECT CLIENTS.* FROM CLIENTS JOIN ACCOUNTS ON CLIENTS.ID = ACCOUNTS.CLIENT_ID" +
            " JOIN BANKS ON BANKS.ID = CLIENTS.BANK_ID GROUP BY CLIENTS.NAME" +
            " HAVING BANKS.NAME = ? ORDER BY SUM(ACCOUNTS.BALANCE)";
    private final String queryClientsByCity =
            "";
    private final String querySaveBank =
            "MERGE INTO BANKS(NAME) KEY(NAME) VALUES(?)";
    private final String queryGetBankID =
            "SELECT ID FROM BANKS WHERE NAME = ?";
    private final String[] queryClearBank = {
            "DELETE FROM ACTIVE_ACCOUNTS",
                    "DELETE FROM ACCOUNTS",
                    "DELETE FROM CLIENTS",
                    "DELETE FROM BANKS"};

    private static BankDAOImpl instance;

    private BankDAOImpl() {

    }

    public static BankDAOImpl getInstance() {
        if (instance == null) {
            instance = new BankDAOImpl();
        }
        return instance;
    }

    @Override
    public Bank getBankByName(String name) throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(queryFindByName);
            ps.setString(1, name);
            rs = ps.executeQuery();
            if (rs.next()) {
                Bank bank = new Bank(name);
                bank.setId(rs.getInt("ID"));
                bank.setClients(DAOFactory.getClientDAO().getAllClients(bank));
                return bank;
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch(SQLException e) {
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex){
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
        return null;
    }

    public void save(Bank bank) throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        PreparedStatement ps = null;
        ResultSet rs = null;
        try {
            ps = conn.prepareStatement(querySaveBank);
            ps.setString(1, bank.getName());
            if (ps.executeUpdate() == 0) {
                throw new DAOException("Impossible to update Account in DB. Transaction is rolled back");
            }
            ps.close();
            ps = conn.prepareStatement(queryGetBankID);
            ps.setString(1, bank.getName());
            rs = ps.executeQuery();
            if (rs == null || !rs.next()) {
                throw new DAOException("Impossible to save in DB. Can't get bankID");
            }
            int bankId = rs.getInt(1);
            bank.setId(bankId);
            DAOFactory.getClientDAO().save(bank, bank.getClients().toArray(new Client[0]));
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (rs != null) {
                try {
                    rs.close();
                } catch (SQLException e){
                }
            }
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex){
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }

    @Override
    public BankInfo getBankInfo(Bank bank) throws DAOException {
        BankInfo info = new BankInfo();
        info.setBankName(bank.getName());

        Set<Client> clientsSet = new TreeSet(new Comparator() {
            public int compare(Object o1, Object o2) {
                int i =  Float.compare(((Client) o1).getSummaryBalance(), ((Client) o2).getSummaryBalance());
                if (i == 0) {
                    return ((Client) o1).getName().compareTo(((Client) o2).getName());
                } else {
                    return i;
                }
            }
        });
        clientsSet.addAll(DAOFactory.getClientDAO().getAllClients(bank));
        info.setClientsSorted(clientsSet);

        Map<String, List<Client>> map = new TreeMap<String, List<Client>>();
        for (Client c: clientsSet) {
            String city = c.getCity().toUpperCase();
            if (!map.containsKey(city)) {
                map.put(city, new ArrayList<Client>());
            }
            map.get(city).add(c);
        }
        info.setClientsByCity(map);

        info.setNumberOfClients(clientsSet.size());

        int n = 0;
        int credit = 0;
        for (Client clients: clientsSet) {
            Set<Account> accounts = clients.getAccounts();
            n += accounts.size();
            for (Account a: accounts) {
                if (a instanceof CheckingAccount) {
                    credit += ((CheckingAccount)a).getCredit();
                }
            }
        }
        info.setAccountsNumber(n);
        info.setBankCreditSum(credit);
        return info;
    }

    @Override
    public void clearBank() throws DAOException {
        Connection conn = DAOFactory.getBaseDAO().openConnection();
        Statement ps = null;
        try {
            ps = conn.createStatement();
            for (String query: queryClearBank) {
                ps.executeUpdate(query);
            }
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        } finally {
            if (ps != null) {
                try {
                    ps.close();
                } catch (SQLException ex){
                }
            }
            DAOFactory.getBaseDAO().closeConnection();
        }
    }
}


