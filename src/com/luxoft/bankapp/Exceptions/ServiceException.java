package com.luxoft.bankapp.Exceptions;

public class ServiceException extends Exception {
    public ServiceException(String msg) {
        super(msg);
    }
}


