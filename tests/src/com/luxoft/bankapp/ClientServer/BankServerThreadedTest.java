package src.com.luxoft.bankapp.ClientServer;

import static org.junit.Assert.*;

import com.luxoft.bankapp.ClientServer.BankServerThreaded;
import com.luxoft.bankapp.DAO.DAOFactory;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.test.service.BankClientMock;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;
import java.util.concurrent.Future;

public class BankServerThreadedTest {
    private Bank bank;
    private final String bankNameByDefault = "Bank №1";
    private final String clientNameByDefault = "Ivan Ivanov";
    private final int N = 1000;

    @Before
    public void initBank() throws Exception {
        bank = DAOFactory.getBankDAO().getBankByName(bankNameByDefault);
        if (bank == null) {
            System.out.println("Bank not found");
            assertTrue(false);
        }
    }

    @Test
    public void withdrawTest() throws Exception {
        Client client = DAOFactory.getClientDAO().findClientByName(bank, clientNameByDefault);
        double amount1 = client.getActiveAccount().getBalance();
        float amount = 1f;

        BankServerThreaded bankServer = BankServerThreaded.getInstance();
        bankServer.init(bank);
        Thread serverThread = new Thread(bankServer);
        serverThread.start();

        ExecutorService executor = Executors.newFixedThreadPool(100);
        List<Future<Long>> clientList = new ArrayList<Future<Long>>();
        for (int i = 0; i < N; i++) {
            Future future = executor.submit(new BankClientMock(client, amount, i + 1));
            clientList.add(future);
        }
        long sum = 0;
        for (int i = 0; i < N; i++) {
            sum += clientList.get(i).get();
        }
        System.out.println("Average waiting time: " + (double)sum/(N*1000) + "s");

        client = DAOFactory.getClientDAO().findClientByName(bank, clientNameByDefault);
        double amount2 = client.getActiveAccount().getBalance();
        assertTrue(amount1 - N == amount2);
    }
}

