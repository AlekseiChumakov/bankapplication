package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;

public class ExitCommand extends AbstractCommand {
    public ExitCommand() {
        super("Exit");
    }

    @Override
    public void execute(BankCommander commander) throws BankException, ServiceException {
        System.exit(0);
    }
}





