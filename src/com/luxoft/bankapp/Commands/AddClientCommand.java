package com.luxoft.bankapp.Commands;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;
import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Model.Gender;
import com.luxoft.bankapp.Services.ServiceFactory;

public class AddClientCommand extends AbstractCommand {
    public final static String EMAIL_PATTERN = "^[_A-Za-z0-9-\\+]+(\\.[_A-Za-z0-9-]+)*@" +
            "[A-Za-z0-9-]+(\\.[A-Za-z0-9]+)*(\\.[A-Za-z]{2,})$";

    public final static String TEL_PATTERN = "\\d{3}-\\d{7}";

    public AddClientCommand() {
        super("Add client");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException, BankException {
        if (BankCommander.currentBank == null) {
            System.out.println("Error! Current bank is undefined");
            return;
        }
        String clientName = fromConsole("add client> Input client name: \n");
        try {
            ServiceFactory.getBankService().getClient(commander.currentBank, clientName);
            System.out.println("Error! Client with such name already exists");
            return;
        } catch (ClientNotFoundException e) {
        }
        String city = fromConsole("add client> Input client city: \n");
        String clientGender;
        while (true) {
            clientGender = fromConsole("add client> Input client gender (M/F): \n");
            if (clientGender.matches("[MmFf]")) {
                break;
            }
        }
        Gender gender = (clientGender.matches("[mM]") ? Gender.MALE : Gender.FEMALE);
        float clientInitialOverdraft;
        while (true) {
            try {
                clientInitialOverdraft = Float.parseFloat(fromConsole("add client> input initial overdraft: \n>"));
                if (clientInitialOverdraft < 0) {
                    System.out.println("Error! Value must be positive\n");
                    continue;
                }
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal overdraft value\n");
                continue;
            }
            break;
        }
        String clientTelephone;
        while (true) {
            clientTelephone = fromConsole("add client> Input client telephone number: \n");
            if (clientTelephone.matches(AddClientCommand.TEL_PATTERN)) {
                break;
            }
        }
        String clientEmail;
        while (true) {
            clientEmail = fromConsole("add client> Input client e-mail: \n");
            if (clientEmail.matches(AddClientCommand.EMAIL_PATTERN)) {
                break;
            }
        }
        Client client = new Client();
        client.setEmail(clientEmail);
        client.setName(clientName);
        client.setInitialOverdraft(clientInitialOverdraft);
        client.setTelephone(clientTelephone);
        client.setGender(gender);
        client.setCity(city);

        ServiceFactory.getBankService().saveClient(commander.currentBank, new Client[] {client});

        commander.setCurrentClient(client);
        System.out.println("Client is added: " + client);
    }
}








