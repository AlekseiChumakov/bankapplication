package com.luxoft.bankapp.Main;

import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.*;

public class BankApplication {
    private final static String bankNameByDefault = "Bank №1";
    private Bank bank;

    private void initialize() {
        try {
            bank = ServiceFactory.getBankService().getBankByName(bankNameByDefault);
        } catch (ServiceException e) {
            System.out.println(e.getMessage());
            System.exit(0);
        }
        if (bank == null) {
            System.out.println("Bank \"" + bankNameByDefault + "\" not found");
            System.exit(0);
        }

    }

    public static void main(final String[] args) {
        BankApplication ba = new BankApplication();
        ba.initialize();
        try {
            System.out.println(ServiceFactory.getBankService().getBankInfo(ba.bank));
        } catch (ServiceException e) {
            System.out.println("Service error! " + e.getMessage());
        }
    }
}








