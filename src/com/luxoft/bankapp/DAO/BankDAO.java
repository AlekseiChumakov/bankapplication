package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Services.BankInfo;

public interface BankDAO {
    Bank getBankByName(String name) throws DAOException;
    BankInfo getBankInfo(Bank bank) throws DAOException;
    void save(Bank bank) throws DAOException;
    void clearBank() throws DAOException;
}


