package com.luxoft.bankapp.Services;

public class ServiceFactory {
    public static BankService getBankService() {
        return BankServiceImpl.getInstance();
    }

    public static ClientService getClientService() {
        return ClientServiceImpl.getInstance();
    }

    public static AccountService getAccountService() {
        return AccountServiceImpl.getInstance();
    }
}

