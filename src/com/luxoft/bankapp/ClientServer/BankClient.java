package com.luxoft.bankapp.ClientServer;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;

public class BankClient {
    static final String SERVER = "localhost";
    static final String clientType = "BANK CLIENT";
    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private int bankClientNumber;

    public BankClient(int n) {
        bankClientNumber = n;
    }

    public void run() {
        try {
            requestSocket = new Socket(SERVER, 2004);
            System.out.println("Connected to " + SERVER);
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());
            out.writeObject(clientType + " №" + bankClientNumber);
            out.flush();
            int commandNumber;
            String message;
            String clientName = null;

            while (true) {
                try {
                    System.out.println("Please select a number of command: ");
                    System.out.println("\"1\" -> Find client");
                    System.out.println("\"2\" -> Get balance");
                    System.out.println("\"3\" -> Withdraw");
                    System.out.println("\"4\" -> Exit");

                    try {
                        commandNumber = Integer.parseInt(fromConsole("Input command number: \n>"));
                    } catch (RuntimeException e) {
                        System.out.println("Error! Illegal number value\n");
                        continue;
                    }

                    switch (commandNumber) {
                        case 1:
                            message = BankServer.findClientPrefix + BankServer.separator + fromConsole("Input client name: \n");
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                clientName = message.substring(BankServer.okPrefix.length());
                                System.out.println("OK! " + clientName + " is selected");
                            } else {
                                if (message.startsWith(BankServer.errorPrefix)) {
                                    System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                                }
                            }
                            continue;
                        case 2:
                            if (clientName == null) {
                                System.out.println("Error! First, select a client");
                                continue;
                            }
                            message = BankServer.getBalancePrefix + BankServer.separator + clientName;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                System.out.println(clientName + " : " + message.substring(BankServer.okPrefix.length()));
                            } else if (message.startsWith(BankServer.errorPrefix)){
                                System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                            }
                            continue;
                        case 3:
                            if (clientName == null) {
                            System.out.println("Error! First, select a client");
                            continue;
                            }
                            float amount;
                            while (true) {
                                try {
                                    amount = Float.parseFloat(fromConsole("Withdraw from " + clientName + ", input amount: \n>"));
                                    if (amount < 0) {
                                        System.out.println("Error! Value must be positive");
                                        continue;
                                    }
                                    break;
                                } catch (RuntimeException e) {
                                    System.out.println("Error! Illegal withdraw value\n");
                                    continue;
                                }
                            }
                            message = BankServer.withdrawPrefix + BankServer.separator + clientName + BankServer.separator + amount;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                System.out.println("OK! " + message.substring(BankServer.okPrefix.length()));
                            } else {
                                System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                            }
                            continue;
                        case 4:
                            message = BankServer.exitPrefix + BankServer.separator;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                System.out.println(message.substring(BankServer.okPrefix.length()));
                                return;
                            } else {
                                System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                            }
                            break;
                        default:
                            System.out.println("Error! Wrong command number");
                            continue;
                    }
                } catch (ClassNotFoundException classNot) {
                    System.err.println("data received in unknown format");
                }
            }
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                requestSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public static void main(final String args[]) {
        BankClient client = new BankClient(1);
        while (true) {
            client.run();
        }
    }
}





