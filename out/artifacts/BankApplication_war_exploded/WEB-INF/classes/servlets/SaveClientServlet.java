import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class SaveClientServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String city = request.getParameter("city");
        String email = request.getParameter("email");
        String balance = request.getParameter("balance");
        String gender = request.getParameter("gender");
        Bank bank = (Bank) getServletContext().getAttribute("bank");
        if (bank == null) {
            logger.warning("No bank specified");
            throw new ServletException("No bank specified");
        }
        Client client;
        Account activeAccount = null;
        try {
            if (!id.equals("")) {
                client = ServiceFactory.getBankService().getClient(bank, Integer.parseInt(id));
                activeAccount = client.getActiveAccount();
            } else {
                client = new Client();
            }
            client.setName(name);
            client.setCity(city);
            client.setEmail(email);
            client.setGender(gender.equals("M") ? Gender.MALE : Gender.FEMALE);

            if (activeAccount != null) {
                ((AbstractAccount) activeAccount).setBalance(Float.parseFloat(balance));
            }
            ServiceFactory.getBankService().saveClient(bank, new Client[]{client});
        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        } catch (ClientNotFoundException e) {
            logger.warning("Client not found");
            throw new ServletException("Client not found");
        } catch (NumberFormatException e) {
            logger.log(Level.SEVERE, "string to number parse error", e);
            throw new ServletException(e.getMessage());
        }
        request.getRequestDispatcher("/client.jsp").forward(request, response);
    }
}
