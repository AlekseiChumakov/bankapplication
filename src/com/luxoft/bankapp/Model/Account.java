package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.test.service.BankObject;

import java.io.Serializable;
import java.util.Map;

public interface Account extends Report, Serializable, BankObject {
    float getBalance();
    void deposit(float x);
    void withdraw(float x) throws NotEnoughFundsException;
    int getId();
    void setId(int id);
    int decimalValue();
    void parseFeed(Map<String, String> feed);
}








