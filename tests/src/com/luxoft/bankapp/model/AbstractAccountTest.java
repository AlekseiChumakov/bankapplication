package src.com.luxoft.bankapp.model;

import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.Model.AbstractAccount;
import org.junit.Test;
import static org.junit.Assert.*;

public class AbstractAccountTest  {
    private class TestingAccount extends AbstractAccount {
        public TestingAccount() {
            super();
        }

        public TestingAccount(int accountNumber) {
            super(accountNumber);
        }

        public TestingAccount(float initialBalance, int accountNumber) {
            super(initialBalance, accountNumber);
        }

        @Override
        public void deposit(float x) {}

        @Override
        public void withdraw(float x) throws NotEnoughFundsException {}

        @Override
        public void printReport(){}
    }
    private TestingAccount sut;

    @Test
    public void testSetBalanceToZeroByDefault() {
        sut = new TestingAccount();

        assertEquals(0.0f, sut.getBalance(), 0);
        assertEquals(0, sut.getId());
    }

    @Test
    public void testSetAccountNumberInConstructor() {
        sut = new TestingAccount(1);

        assertEquals(0, sut.getBalance(), 0);
        assertEquals(1, sut.getId(), 0);
    }

    @Test
    public void testSetBalanceAndAccountNumberInConstructor() {
        sut = new TestingAccount(100.0f, 1);

        assertEquals(100.0f, sut.getBalance(), 0);
        assertEquals(1, sut.getId(), 0);
    }

    @Test(expected = IllegalArgumentException.class)
    public void testNegativeThrowsException() {
        sut = new TestingAccount(-100, 1);
    }
}


