import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.test.service.TestService;
import org.junit.Test;

import java.util.HashSet;
import java.util.Set;

import static org.junit.Assert.*;

public class TestServiceTest {
    @Test
    public void accountsEqualsTest() throws Exception {
        Account account1 = new SavingAccount(100f, 1);     //  SavingAccount(balance, id)
        Account account2 = new SavingAccount(100f, 2);
        assertTrue(TestService.isEquals(account1, account2));
        ((AbstractAccount)account1).setBalance(120f);
        assertFalse(TestService.isEquals(account1, account2));

        Account account3 = new CheckingAccount(100f, 200f, 3);     //  CheckingAccount(balance, overdraft, id)
        Account account4 = new CheckingAccount(100f, 200f, 4);
        assertTrue(TestService.isEquals(account3, account4));
        ((CheckingAccount)account3).setOverdraftStartValue(220f);
        assertFalse(TestService.isEquals(account3, account4));
        Account account5 = new CheckingAccount(120f, 200f, 5);
        assertFalse(TestService.isEquals(account4, account5));

        Account account6 = new CheckingAccount(100f, 200f, 150f, 6); //  CheckingAccount(balance, overdraft, current overdraft, id)
        Account account7 = new CheckingAccount(100f, 200f, 150f, 7);
        assertTrue(TestService.isEquals(account6, account7));
        ((CheckingAccount)account6).setOverdraftCurrentValue(160);
        assertFalse(TestService.isEquals(account6, account7));
    }

    @Test
    public void clientsEqualsTest() throws Exception {
        Client client1 = new Client();
        client1.setId(1);
        client1.setName("Вася");
        client1.setGender(Gender.MALE);
        client1.setTelephone("067-1234567");
        client1.setEmail("abc@def.gh");
        client1.setInitialOverdraft(1000);
        client1.setCity("Киев");
        Account account1 = new SavingAccount(200f, 1);  //  SavingAccount(balance, id)
        Account account2 = new CheckingAccount(300f, 100f, 80f, 2); //  CheckingAccount(balance, overdraft, current overdraft, id)
        Set<Account> accounts1 = new HashSet<Account>();
        accounts1.add(account1);
        accounts1.add(account2);
        client1.setAccounts(accounts1);
        client1.setActiveAccount(account2);

        Client client2 = new Client();
        client2.setId(2);
        client2.setName("Вася");
        client2.setGender(Gender.MALE);
        client2.setTelephone("067-1234567");
        client2.setEmail("abc@def.gh");
        client2.setInitialOverdraft(1000);
        client2.setCity("Киев");
        Account account3 = new SavingAccount(200f, 1);  //  SavingAccount(balance, id)
        Account account4 = new CheckingAccount(300f, 100f, 80f, 2); //  CheckingAccount(balance, overdraft, current overdraft, id)
        Set<Account> accounts2 = new HashSet<Account>();
        accounts2.add(account3);
        accounts2.add(account4);
        client2.setAccounts(accounts2);
        client2.setActiveAccount(account4);
        assertTrue(TestService.isEquals(client1, client2));

        client2.setActiveAccount(account3);
        assertFalse(TestService.isEquals(client1, client2));
        client2.setActiveAccount(account4);
        assertTrue(TestService.isEquals(client1, client2));

        ((AbstractAccount)client2.getActiveAccount()).setBalance(333f);
        assertFalse(TestService.isEquals(client1, client2));
        ((AbstractAccount)client2.getActiveAccount()).setBalance(300f);
        assertTrue(TestService.isEquals(client1, client2));

        client2.setName("Петя");
        assertFalse(TestService.isEquals(client1, client2));
        client2.setName("Вася");
        assertTrue(TestService.isEquals(client1, client2));

        client2.setGender(Gender.FEMALE);
        assertFalse(TestService.isEquals(client1, client2));
        client2.setGender(Gender.MALE);
        assertTrue(TestService.isEquals(client1, client2));

        client2.setGender(Gender.FEMALE);
        assertFalse(TestService.isEquals(client1, client2));
        client2.setGender(Gender.MALE);
        assertTrue(TestService.isEquals(client1, client2));

        client2.setTelephone("067-7654321");
        assertFalse(TestService.isEquals(client1, client2));
        client2.setTelephone("067-1234567");
        assertTrue(TestService.isEquals(client1, client2));

        client2.setEmail("zxc@vb.er");
        assertFalse(TestService.isEquals(client1, client2));
        client2.setEmail("abc@def.gh");
        assertTrue(TestService.isEquals(client1, client2));

        client2.setCity("Одесса");
        assertFalse(TestService.isEquals(client1, client2));
        client2.setCity("Киев");
        assertTrue(TestService.isEquals(client1, client2));

        client2.setInitialOverdraft(2000);
        assertFalse(TestService.isEquals(client1, client2));
        client2.setInitialOverdraft(1000);
        assertTrue(TestService.isEquals(client1, client2));
    }

    @Test
    public void banksEqualsTest() throws Exception {
        Bank bank1 = new Bank("Bank 1");
        bank1.setId(1);
        Bank bank2 = new Bank("Bank 1");
        bank2.setId(2);
        assertTrue(TestService.isEquals(bank1, bank2));

        bank2.setName("Bank 2");
        assertFalse(TestService.isEquals(bank1, bank2));
        bank2.setName("Bank 1");
        assertTrue(TestService.isEquals(bank1, bank2));

        Client client1 = new Client();
        client1.setName("Вася");
        client1.setId(1);
        bank1.addClient(client1);

        Client client2 = new Client();
        client2.setName("Вася");
        client1.setId(2);
        bank2.addClient(client2);

        assertTrue(TestService.isEquals(bank1, bank2));
        client2.setName("Петя");
        assertFalse(TestService.isEquals(bank1, bank2));
        client2.setName("Вася");
        assertTrue(TestService.isEquals(bank1, bank2));

        Client client3 = new Client("Витя");
        client3.setId(3);
        bank1.addClient(client3);
        assertFalse(TestService.isEquals(bank1, bank2));
        bank1.removeClient(client3);
        assertTrue(TestService.isEquals(bank1, bank2));

        Account account1 = new SavingAccount(100f, 1); //  SavingAccount(balance, id)
        client1.addAccount(account1);
        Account account2 = new SavingAccount(100f, 2); //  SavingAccount(balance, id)
        client2.addAccount(account2);
        assertTrue(TestService.isEquals(bank1, bank2));

        Account account3 = new CheckingAccount();
        ((CheckingAccount)account3).setOverdraftStartValue(5000);
        Account account4 = new CheckingAccount();
        ((CheckingAccount)account4).setOverdraftStartValue(5000);

        client1.addAccount(account3);
        assertFalse(TestService.isEquals(bank1, bank2));
        client2.addAccount(account4);
        assertTrue(TestService.isEquals(bank1, bank2));

        ((AbstractAccount)account1).setBalance(700);
        assertFalse(TestService.isEquals(bank1, bank2));
        ((AbstractAccount)account2).setBalance(700);
        assertTrue(TestService.isEquals(bank1, bank2));

        ((CheckingAccount)account3).setOverdraftCurrentValue(1700);
        assertFalse(TestService.isEquals(bank1, bank2));
        ((CheckingAccount)account4).setOverdraftCurrentValue(1700);
        assertTrue(TestService.isEquals(bank1, bank2));
    }
}

