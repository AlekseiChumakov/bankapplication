import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpServlet;
import javax.servlet.ServletOutputStream;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class WelcomeServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        String bankName = config.getInitParameter("bankName");
        if (bankName == null) {
            logger.warning("No bank specified");
            throw new ServletException("No bank specified");
        }
        Bank bank = null;
        try {
            bank = ServiceFactory.getBankService().getBankByName(bankName);
        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        }
        if (bank == null) {
            logger.warning("Bank not found");
            throw new ServletException("Bank not found");
        }
        getServletContext().setAttribute("bank", bank);
    }

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws IOException {
        ServletOutputStream out = response.getOutputStream();
        response.setContentType("text/html; charset=UTF-8");
        out.println("Hello! I'm ATM! <br>");
        out.println("<a href='login.html'>login</a>");
    }
}