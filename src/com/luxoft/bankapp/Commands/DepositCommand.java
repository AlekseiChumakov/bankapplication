package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Services.ServiceFactory;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;

public class DepositCommand extends AbstractCommand {
    public DepositCommand() {
        super("Deposit");
    }

    @Override
    public void execute(BankCommander commander) throws  BankException, ServiceException {
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined\n");
            return;
        }
        if (commander.getCurrentClient().getActiveAccount() == null) {
            System.out.println("Error! Active client account is undefined\n");
            return;
        }
        float amount;
        while (true) {
            try {
                amount = Float.parseFloat(fromConsole("Deposit to " + commander.getCurrentClient().getClientSalutation()
                        + ", input amount: \n>"));
                if (amount < 0) {
                    System.out.println("Error! Value must be positive");
                    continue;
                }
                break;
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal deposit value\n");
                continue;
            }
        }
        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(commander.getCurrentClient().getName());
        ServiceFactory.getAccountService().deposit(activeAccount, amount);
        System.out.println(commander.getCurrentClient().getName() + " deposit " + amount + " on his active account.\n");
    }
}









