package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.Exceptions.NotEnoughFundsException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Account;

public interface AccountService {
    void deposit(Account account, float amount) throws ServiceException;
    void withdraw(Account account, float amount) throws ServiceException, NotEnoughFundsException;
    void transfer(Account account1, Account account2, float amount) throws ServiceException, NotEnoughFundsException;
}

