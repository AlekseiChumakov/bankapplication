package com.luxoft.bankapp.test.service;

import com.luxoft.bankapp.Annotations.NoDB;
import com.luxoft.bankapp.Model.AbstractAccount;
import com.luxoft.bankapp.Model.Account;

import java.beans.PropertyDescriptor;
import java.lang.reflect.Field;
import java.util.*;

public class TestService {
    public static boolean isEquals(Object o1, Object o2) throws Exception {
        if (o1 == null || o2 == null) {
            return Objects.equals(o1, o2);
        }
        if (o1 == o2) {
            return true;
        }
        Class class1 = o1.getClass();
        if (class1 != o2.getClass()) {
            return false;
        }
        if (o1 instanceof BankObject) {
            if (class1 == Account.class) {
                class1 = AbstractAccount.class;
            }
            do {
                Field[] fields = class1.getDeclaredFields();
                for (Field field : fields) {
                    if (field.getAnnotation(NoDB.class) == null) {
                        Object value1 = new PropertyDescriptor(field.getName(), class1).getReadMethod().invoke(o1);
                        Object value2 = new PropertyDescriptor(field.getName(), class1).getReadMethod().invoke(o2);
                        if (!isEquals(value1, value2)) {
                            return false;
                        }
                    }
                }
                class1 = class1.getSuperclass();
            } while (BankObject.class.isAssignableFrom(class1));
        } else if (o1 instanceof Collection) {
            if (((Collection) o1).size() != ((Collection) o2).size()) {
                return false;
            }
            Iterator iterator1 = ((Collection) o1).iterator();
            Iterator iterator2 = ((Collection) o2).iterator();

            while (iterator1.hasNext()) {
                if (!isEquals(iterator1.next(), iterator2.next())) {
                    return false;
                }
            }
        } else if (o1 instanceof Map) {
            if (((Map) o1).size() != ((Map) o2).size()) {
                Set set1 = ((Map)o1).keySet();
                Set set2 = ((Map)o2).keySet();
                Iterator i1 = set1.iterator();
                Iterator i2 = set2.iterator();
                while (i1.hasNext()) {
                    Object obj1 = i1.next();
                    Object obj2 = i2.next();
                    if (!isEquals(obj1, obj2) ||
                            !isEquals(((Map)o1).get(obj1), ((Map)o2).get(obj2))) {
                        return false;
                    }
                }
                return false;
            }
        } else {
            if (!o1.equals(o2)) {
                return false;
            }
        }
        return true;
    }
}

