package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Annotations.NoDB;
import com.luxoft.bankapp.Exceptions.AccountExistsException;
import com.luxoft.bankapp.Exceptions.FeedException;
import com.luxoft.bankapp.test.service.BankObject;

import java.io.Serializable;
import java.util.*;

public class Client implements Report, Serializable, BankObject {
    @NoDB private int id;
    private String name;
    private Account activeAccount;
    private float initialOverdraft;
    private String telephone;
    private String email;
    private String city;
    private Gender gender;
    private Set<Account> accounts;

    public Client() {
        accounts = new HashSet<Account>();
    }

    public Client(String name) {
        this();
        this.name = name;
    }

    @Override
    public void printReport() {
        System.out.println(getClientSalutation());
        for (Account a : accounts) {
            a.printReport();
        }
        System.out.println("");
    }

    public Set<Account> getAccounts() {
        return Collections.unmodifiableSet(accounts);
    }

    public void setAccounts(Set<Account> accounts) {
        this.accounts = accounts;
    }

    public void setName(String n) {
        name = n;
    }

    public String getName() {
        return name;
    }

    public void setGender(Gender g) {
        gender = g;
    }

    public float getInitialOverdraft() {
        return initialOverdraft;
    }

    public void setInitialOverdraft(float x) {
        initialOverdraft = x;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String str) {
        email = str;
    }

    public String getTelephone() {
        return telephone;
    }

    public void setTelephone(String str) {
        telephone = str;
    }

    public void setCity(String s) {
        city = s;
    }

    public String getCity() {
        return city;
    }

    public void setActiveAccount(Account a) {
        activeAccount = a;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Account getActiveAccount() {
        return activeAccount;
    }

    private Account createAccount(String accountType) {
        Account acc;
        if ("s".equalsIgnoreCase(accountType)) {
            acc = new SavingAccount();
        } else if ("c".equalsIgnoreCase(accountType)) {
            acc = new CheckingAccount();
        } else {
            throw new FeedException("Account type not found \"" + accountType + "\"");
        }
        accounts.add(acc);
        return acc;
    }

    public void addAccount(Account a) throws AccountExistsException {
        if (!accounts.add(a)) {
            throw new AccountExistsException();
        }
    }

    public String getClientSalutation() {
        return gender.convertToString() + name;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (this == obj) {
            return true;
        }
        if (!(obj instanceof Client)) {
            return false;
        }
        Client cl = (Client) obj;
        if (getName() == null || cl.getName() == null) {
            return Objects.equals(getName(), cl.getName());
        }
        return getName().equals(cl.getName());
    }

    @Override
    public int hashCode() {
        if (getName() == null) {
            return 0;
        }
        return getName().hashCode();
    }

    @Override
    public String toString() {
        StringBuilder str = new StringBuilder("Name: ");
        str.append(getClientSalutation()).append("; city: ").append(city)
        .append("; tel. ").append(telephone).append("; e-mail: ").append(email).append("\n");
        for (Account a : accounts) {
            str.append(a.toString());
            if (getActiveAccount() == a) {
                str.append("  = ACTIVE =");
            }
            str.append("\n");
        }
        return str.toString();
    }

    public float getSummaryBalance() {
        float result = 0;
        for (Account a : getAccounts()) {
            result += a.getBalance();
        }
        return result;
    }

    public void parseFeed(Map<String, String> feed) {
        String accountType = feed.get("accounttype");
        Account acc = createAccount(accountType);
        acc.parseFeed(feed);
    }
}








