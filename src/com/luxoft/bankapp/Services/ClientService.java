package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.Exceptions.AccountExistsException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.util.List;

public interface ClientService {
    void saveAccount(Client client, Account[] accounts) throws ServiceException, AccountExistsException;
    void setActiveAccount(Client client, Account account) throws ServiceException;
    List<Account> getAccounts(int clientId, Bank bank) throws ServiceException;
    Account getActiveAccount(String clientName) throws ServiceException;
}

