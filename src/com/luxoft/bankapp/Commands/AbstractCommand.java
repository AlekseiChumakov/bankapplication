package com.luxoft.bankapp.Commands;

public abstract class AbstractCommand implements Command {
    private String commandName;

    AbstractCommand(String name) {
        commandName = name;
    }

    @Override
    public void printCommandInfo() {
        System.out.print(commandName);
    }
}





