import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.CheckingAccount;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.ServletOutputStream;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class BalanceServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        HttpSession session = request.getSession();
        String clientName = (String)session.getAttribute("clientName");
        if (clientName == null) {
            logger.warning("No client specified");
            throw new ServletException("No client specified");
        }
        Bank bank = (Bank)getServletContext().getAttribute("bank");
        if (bank == null) {
            logger.warning("No bank specified");
            throw new ServletException("No bank specified");
        }
        float balance;
        Account activeAccount;
        try {
            activeAccount = ServiceFactory.getBankService().getClient(bank, clientName).getActiveAccount();
            if (activeAccount == null) {
                logger.warning("Active account is undefined");
                throw new ServletException("Active account for " + clientName + " is undefined");
            }
            balance = activeAccount.getBalance();
        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        } catch (ClientNotFoundException e) {
            logger.warning("Client not found");
            throw new ServletException("Client not found");
        }
        request.setAttribute("balance", balance);
        if (activeAccount instanceof CheckingAccount) {
            request.setAttribute("overdraft", ((CheckingAccount) activeAccount).getOverdraftCurrentValue());
        }
        request.getRequestDispatcher("/balance.jsp").forward(request, response);
    }
}
