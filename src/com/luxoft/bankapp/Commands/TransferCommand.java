package com.luxoft.bankapp.Commands;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;
import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.ServiceFactory;

public class TransferCommand extends AbstractCommand {
    public TransferCommand() {
        super("Transfer");
    }

    @Override
    public void execute(BankCommander commander) throws BankException, ServiceException {
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined\n");
            return;
        }
        System.out.println("Current client: " +
                ServiceFactory.getBankService().getClient(commander.currentBank, commander.getCurrentClient().getName()));
        Account account1 = ServiceFactory.getClientService().getActiveAccount(commander.getCurrentClient().getName());
        if (account1 == null) {
            System.out.println("Error! Active client account is undefined\n");
            return;
        }
        String client2Name = fromConsole("Input second client name: \n");
        Client client2 = ServiceFactory.getBankService().getClient(BankCommander.currentBank, client2Name);
        if (commander.getCurrentClient().equals(client2)) {
            System.out.println("Error! Current client == second client\n");
            return;
        }
        System.out.println("Client found: " + client2);
        Account account2 = ServiceFactory.getClientService().getActiveAccount(client2.getName());
        if (account2 == null) {
            System.out.println("Error! Second client account is undefined\n");
            return;
        }
        float amount;
        while (true) {
            try {
                amount = Float.parseFloat(fromConsole("Transfer from " + commander.getCurrentClient().getClientSalutation()
                        + " to " + client2.getClientSalutation() + ", input amount: \n>"));
                if (amount < 0) {
                    System.out.println("Error! Value must be positive");
                    continue;
                }
                break;
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal transfer value\n");
                continue;
            }
        }
        ServiceFactory.getAccountService().transfer(account1, account2, amount);

        System.out.println("Transferred " + amount + " from " + commander.getCurrentClient().getName() +
                 " to " + client2.getName() + " successfully\n");
    }

}








