package com.luxoft.bankapp.Exceptions;

import com.luxoft.bankapp.Model.Account;

public class OverDraftLimitExceededException extends NotEnoughFundsException {
    private Account causeAccount;
    private float amount;

    public OverDraftLimitExceededException(Account a, float amount) {
        causeAccount = a;
        this.amount = amount;
    }

    @Override
    public String getMessage() {
        return "Account №" + causeAccount.getId()+ " available money: " + amount;
    }
}

