package com.luxoft.bankapp.ClientServer;

import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Model.Gender;
import com.luxoft.bankapp.Services.*;
import sun.misc.BASE64Encoder;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;

public class BankServer {
    private final static String bankNameByDefault = "Bank №1";

    public static final String separator = "=";
    public static final String findClientPrefix = "findclient";
    public static final String getBalancePrefix = "getbalance";
    public static final String withdrawPrefix = "withdraw";
    public static final String exitPrefix = "bye";
    public static final String okPrefix = "OK!";
    public static final String errorPrefix = "ERROR!";
    public static final String bankInfoPrefix = "bankinfo";
    public static final String clientInfoPrefix = "clientinfo";
    public static final String addClientPrefix = "addclient";
    public static final String removeClientPrefix = "removeclient";
    public static final String readyPrefix = "ready";

    private ServerSocket providerSocket;
    private Socket connection;
    private ObjectOutputStream out;
    private ObjectInputStream in;
    private Bank currentBank;

    public void initialize() {
        try {
            currentBank = ServiceFactory.getBankService().getBankByName(bankNameByDefault);
        } catch (ServiceException e) {
            System.out.println("Service error! " + e.getMessage());
            System.exit(0);
        }
        if (currentBank == null) {
            System.out.println("Bank \"" + bankNameByDefault + "\" not found\n");
            System.exit(0);
        }
    }

   public  void run() {
        try {
            providerSocket = new ServerSocket(2004, 10);
            System.out.println("Waiting for connection ...");
            connection = providerSocket.accept();
            System.out.println("Connection received from "
                    + connection.getInetAddress().getHostName());
            out = new ObjectOutputStream(connection.getOutputStream());
            out.flush();
            in = new ObjectInputStream(connection.getInputStream());
            System.out.println((String)in.readObject() + " connected");
            String message;

            while (true) {
                try {
                    message = (String)in.readObject();
                    System.out.println("from client: " + message);                   // for logging
                    String[] splitMessage = message.split(separator);
                    if (splitMessage[0].equals(findClientPrefix)) {
                        try {
                            message = okPrefix + ServiceFactory.getBankService().getClient(currentBank, splitMessage[1]).getName();
                        } catch (ClientNotFoundException e) {
                            System.out.println("server: client not found with name " + splitMessage[1]);
                            message = errorPrefix + "Client not found";
                        }
                        out.writeObject(message);
                        out.flush();
                    } else if (splitMessage[0].equals(getBalancePrefix)) {
                        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(splitMessage[1]);
                        if (activeAccount == null) {
                            System.out.println("server: no active account for " + splitMessage[0]);
                            message = errorPrefix + "No active account for this client";
                            out.writeObject(message);
                            out.flush();
                            continue;
                        }
                        message = okPrefix + activeAccount;
                        out.writeObject(message);
                        out.flush();
                    } else if (splitMessage[0].equals(withdrawPrefix)) {
                        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(splitMessage[1]);
                        if (activeAccount == null) {
                            System.out.println("server: no active account for " + splitMessage[0]);
                            message = errorPrefix + "No active account for this client";
                            out.writeObject(message);
                            out.flush();
                            continue;
                        }
                        float amount = Float.parseFloat(splitMessage[2]);
                        try {
                            ServiceFactory.getAccountService().withdraw(activeAccount, amount);
                            message = okPrefix + "Successfully! " + activeAccount;
                        } catch (OverDraftLimitExceededException e) {
                            message = errorPrefix + "Overdraft limit exceeded";
                        } catch (NotEnoughFundsException e) {
                            message = errorPrefix + "No enough funds";
                        } catch (BankException e) {
                            message = errorPrefix + e.getMessage();
                        } finally {
                            out.writeObject(message);
                            out.flush();
                            continue;
                        }
                    } else if (splitMessage[0].equals(exitPrefix)) {
                        message = okPrefix + "Bye!";
                        out.writeObject(message);
                        out.flush();
                        return;
                    } else if (splitMessage[0].equals(bankInfoPrefix)) {
                        message = okPrefix + ServiceFactory.getBankService().getBankInfo(currentBank);
                        out.writeObject(message);
                        out.flush();
                        continue;
                    } else if (splitMessage[0].equals(clientInfoPrefix)) {
                        try {
                            Client client =  ServiceFactory.getBankService().getClient(currentBank, splitMessage[1]);
                            ByteArrayOutputStream baos = new ByteArrayOutputStream();
                            ObjectOutputStream oos = new ObjectOutputStream(baos);
                            oos.writeObject(client);
                            baos.flush();
                            StringBuilder sb = new StringBuilder(new BASE64Encoder().encode(baos.toByteArray()));
                            oos.close();
                            baos.close();
                            sb.insert(0, okPrefix);
                            out.writeObject(sb.toString());
                            out.flush();
                        } catch (ClientNotFoundException e) {
                            System.out.println("server: client not found with name " + splitMessage[1]);
                            message = errorPrefix + "Client not found";
                            out.writeObject(message);
                            out.flush();
                        }
                        continue;
                    } else if (splitMessage[0].equals(addClientPrefix)) {
                        Client client = new Client();
                        client.setName(splitMessage[1]);
                        client.setGender(splitMessage[2].matches("[Mm]") ? Gender.MALE : (splitMessage[2].matches("[Ff]") ? Gender.FEMALE : null));
                        client.setCity(splitMessage[3]);
                        client.setTelephone(splitMessage[4]);
                        client.setEmail(splitMessage[5]);
                        client.setInitialOverdraft(Float.parseFloat(splitMessage[6]));
                        try {
                            ServiceFactory.getBankService().getClient(currentBank, client.getName());
                            ServiceFactory.getBankService().saveClient(currentBank, new Client[]{client});
                            message = okPrefix + "Client was added successfully";
                        } catch (BankException e) {
                            message = errorPrefix + e.getMessage();
                        }
                        out.writeObject(message);
                        out.flush();
                        continue;
                    } else if (splitMessage[0].equals(removeClientPrefix)) {
                        BankService bs = ServiceFactory.getBankService();
                        Client client;
                        try {
                            client = bs.getClient(currentBank, splitMessage[1]);
                            bs.removeClient(currentBank, client);
                            message = okPrefix + client.getClientSalutation() + " successfully removed";
                        } catch (ClientNotFoundException e) {
                            message = errorPrefix + "Client not found";
                        }
                        out.writeObject(message);
                        out.flush();
                        continue;
                    }
                } catch (ClassNotFoundException classnot) {
                    System.err.println("Data received in unknown format");
                } catch (ServiceException e) {
                    System.out.println("Service error! " + e.getMessage());
                    System.exit(0);
                }
            }
        } catch (IOException  ioException) {
            ioException.printStackTrace();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                providerSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public static void main(final String args[]) {
        BankServer server = new BankServer();
        server.initialize();

        while (true) {
            server.run();
        }
    }
}





