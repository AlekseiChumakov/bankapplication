package com.luxoft.bankapp.ClientServer;

public class BankServerMonitor implements Runnable {
    @Override
    public void run() {
        while (true) {
            System.out.println("waiting clients number: " +
                    BankServerThreaded.getInstance().waitingClientsNumber.get());
            try {
                Thread.sleep(10000);
            } catch (InterruptedException e) {
                System.out.println("BankServer monitor is interrupted");
                break;
            }
        }
    }
}

