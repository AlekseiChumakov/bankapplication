package com.luxoft.bankapp.Commands;

import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Main.BankCommander;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Services.ServiceFactory;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;

public class WithdrawCommand extends AbstractCommand {
    public WithdrawCommand() {
        super("Withdraw");
    }

    @Override
    public void execute(BankCommander commander) throws ServiceException, NotEnoughFundsException {
        if (commander.getCurrentClient() == null) {
            System.out.println("Error! Current client is undefined\n");
            return;
        }
        Account activeAccount = ServiceFactory.getClientService().getActiveAccount(commander.getCurrentClient().getName());
        if (activeAccount == null) {
            System.out.println("Error! Active client account is undefined\n");
            return;
        }
        float amount;
        while (true) {
            try {
                amount = Float.parseFloat(fromConsole("Withdraw from " +
                        commander.getCurrentClient().getName() + ", input amount: \n>"));
                if (amount < 0) {
                    System.out.println("Error! Value must be positive");
                    continue;
                }
                break;
            } catch (RuntimeException e) {
                System.out.println("Error! Illegal withdraw value\n");
                continue;
            }
        }
        ServiceFactory.getAccountService().withdraw(activeAccount, amount);

        System.out.println(commander.getCurrentClient().getName() + " withdraw " + amount + " from his active account\n");
    }
}








