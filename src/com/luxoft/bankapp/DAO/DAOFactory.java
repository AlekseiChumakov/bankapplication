package com.luxoft.bankapp.DAO;

public class DAOFactory {
    public static BaseDAO getBaseDAO() {
        return BaseDAOImpl.getInstance();
    }

    public static BankDAO getBankDAO() {
        return BankDAOImpl.getInstance();
    }

    public static ClientDAO getClientDAO() {
        return ClientDAOImpl.getInstance();
    }

    public static AccountDAO getAccountDAO() {
        return AccountDAOImpl.getInstance();
    }
}

