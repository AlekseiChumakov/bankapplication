function checkLoginForm() {
    var name=$("#user").val();
    if (name.length < 2) {
        $("#nameError").html("пожалуйста, укажите имя");
        return false;
    }
    return true;
}

function checkAmount(value) {
    var amount=value.val();
    if (!isNumeric(amount) || amount < 0 || amount == 0) {
        $("#amountError").html("пожалуйста, введите корректную сумму");
        return false;
    }
    return true;
}

function checkAddClientForm() {
    var name = $("#name").val();
    var nameArray = name.split(" ");
    if (nameArray.length < 2) {
        $("#nameError").html("введите имя и фамилию!");
        return false;
    }
    for(var i = 0; i < nameArray.length; i++) {
        var n = nameArray[i];
        if (n.length < 2) {
          $("#nameError").html("слишком коротое имя или фамилия");
          return false;
        }
    }
    var city = $("#city").val();
    if (city.length < 1) {
         $("#cityError").html("введите название города");
         return false;
    }
    var email = $("#email").val();
    if (!email.match(".+@.+\.[a-zA-Z]{2,3}")) {
        $("#emailError").html("неверный формат e-mail");
        return false;
    }
    var balance=$("#balance").val();
    if (!isNumeric(balance) || balance < 0) {
        $("#balanceError").html("пожалуйста, введите корректное значение баланса");
        return false;
    }
    return true;
}

function isNumeric(input) {
    var RE = /^-{0,1}\d*\.{0,1}\d+$/;
    return (RE.test(input));
}

function hideHints() {
    $(".hints").empty();
}

