package com.luxoft.bankapp.ClientServer;

import static com.luxoft.bankapp.Main.BankCommander.fromConsole;
import com.luxoft.bankapp.Commands.AddClientCommand;
import com.luxoft.bankapp.Model.Client;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;
import java.net.UnknownHostException;
import sun.misc.BASE64Decoder;

public class BankRemoteOffice {
    static public final String SERVER = "localhost";
    static public final String clientType = "REMOTE OFFICE";
    private Socket requestSocket;
    private ObjectOutputStream out;
    private ObjectInputStream in;

    private String getClientInfo(String message) {
        ObjectInputStream ois = null;
        Client client = null;
        try {
            byte[] data = new BASE64Decoder().decodeBuffer(message);
            ois  = new ObjectInputStream(new ByteArrayInputStream(data));
            client = (Client) ois.readObject();
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            try {
                ois.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
        if (client != null) {
            return client.toString();
        }
        return null;
    }

    public void run() {
        try {
            requestSocket = new Socket(SERVER, 2004);
            System.out.println("Connected to " + SERVER);
            out = new ObjectOutputStream(requestSocket.getOutputStream());
            out.flush();
            in = new ObjectInputStream(requestSocket.getInputStream());
            out.writeObject(clientType);
            out.flush();
            int commandNumber;
            String message;

            while (true) {
                try {
                    System.out.println("Please select a number of command: ");
                    System.out.println("\"1\" -> Bank info");
                    System.out.println("\"2\" -> Client info");
                    System.out.println("\"3\" -> Add client");
                    System.out.println("\"4\" -> Remove client");
                    System.out.println("\"5\" -> Exit");

                    try {
                        commandNumber = Integer.parseInt(fromConsole("Input command number: \n>"));
                    } catch (RuntimeException e) {
                        System.out.println("Error! Illegal number value\n");
                        continue;
                    }

                    switch (commandNumber) {
                        case 1:
                            message = BankServer.bankInfoPrefix + BankServer.separator;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                message = message.substring(BankServer.okPrefix.length());
                                System.out.println(message);
                            } else {
                                if (message.startsWith(BankServer.errorPrefix)) {
                                    System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                                }
                            }
                            continue;
                        case 2:
                            message = BankServer.clientInfoPrefix + BankServer.separator + fromConsole("Input client name: \n");
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                message = message.substring(BankServer.okPrefix.length());
                                System.out.println(getClientInfo(message));
                            } else {
                                if (message.startsWith(BankServer.errorPrefix)) {
                                    System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                                }
                            }
                            continue;
                        case 3:
                            String clientName = fromConsole("add client> Input client name: \n");
                            String clientGender;
                            String clientCity;
                            String clientTelephone;
                            String clientEmail;
                            float clientInitialOverdraft;
                            while (true) {
                                clientGender = fromConsole("add client> Input client gender (M/F): \n");
                                if (clientGender.matches("[MmFf]")) {
                                    break;
                                }
                            }
                            clientCity = fromConsole("add client> Input client city: \n");
                            while (true) {
                                clientTelephone = fromConsole("add client> Input client telephone number: \n");
                                if (clientTelephone.matches(AddClientCommand.TEL_PATTERN)) {
                                    break;
                                }
                            }
                            while (true) {
                                clientEmail = fromConsole("add client> Input client e-mail: \n");
                                if (clientEmail.matches(AddClientCommand.EMAIL_PATTERN)) {
                                    break;
                                }
                            }
                            while (true) {
                                try {
                                    clientInitialOverdraft = Float.parseFloat(fromConsole("add client> input initial overdraft: \n>"));
                                    if (clientInitialOverdraft < 0) {
                                        System.out.println("Error! Value must be positive\n");
                                        continue;
                                    }
                                } catch (RuntimeException e) {
                                    System.out.println("Error! Illegal overdraft value\n");
                                    continue;
                                }
                                break;
                            }
                            StringBuilder sb = new StringBuilder(BankServer.addClientPrefix).append(BankServer.separator);
                            sb.append(clientName).append(BankServer.separator);
                            sb.append(clientGender).append(BankServer.separator);
                            sb.append(clientCity).append(BankServer.separator);
                            sb.append(clientTelephone).append(BankServer.separator);
                            sb.append(clientEmail).append(BankServer.separator);
                            sb.append(clientInitialOverdraft).append(BankServer.separator);
                            out.writeObject(sb.toString());
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                message = message.substring(BankServer.okPrefix.length());
                                System.out.println("OK! " + message);
                            } else {
                                if (message.startsWith(BankServer.errorPrefix)) {
                                    System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                                }
                            }
                            continue;
                        case 4:
                            clientName = fromConsole("Input client name for removing: \n");
                            message = BankServer.removeClientPrefix + BankServer.separator + clientName + BankServer.separator;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                message = message.substring(BankServer.okPrefix.length());
                                System.out.println("OK! " + message);
                            } else {
                                if (message.startsWith(BankServer.errorPrefix)) {
                                    System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                                }
                            }
                            continue;
                        case 5:
                            message = BankServer.exitPrefix + BankServer.separator;
                            out.writeObject(message);
                            out.flush();
                            message = (String)in.readObject();
                            if (message.startsWith(BankServer.okPrefix)) {
                                System.out.println(message.substring(BankServer.okPrefix.length()));
                                return;
                            } else {
                                System.out.println("Error! " + message.substring(BankServer.errorPrefix.length()));
                            }
                            break;
                        default:
                            System.out.println("Error! Wrong command number");
                            continue;
                    }
                } catch (ClassNotFoundException classNot) {
                    System.err.println("data received in unknown format");
                }
            }
        } catch (UnknownHostException unknownHost) {
            System.err.println("You are trying to connect to an unknown host!");
        } catch (IOException ioException) {
            ioException.printStackTrace();
        } finally {
            try {
                in.close();
                out.close();
                requestSocket.close();
            } catch (IOException ioException) {
                ioException.printStackTrace();
            }
        }
    }

    public static void main(final String args[]) {
        BankRemoteOffice office = new BankRemoteOffice();
        while(true) {
            office.run();
        }
    }
}




