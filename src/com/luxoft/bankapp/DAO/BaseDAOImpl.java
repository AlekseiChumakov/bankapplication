package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.DAOException;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class BaseDAOImpl implements BaseDAO {
    private static BaseDAOImpl instance;

    private BaseDAOImpl() {

    }

    public static BaseDAOImpl getInstance() {
        if (instance == null) {
            instance = new BaseDAOImpl();
        }
        return instance;
    }

    private static final String dbURL = "jdbc:h2:tcp://localhost/~/IdeaProjects/BankApplication/bank1";
    private Connection conn;

    @Override
    public Connection openConnection() throws DAOException {
        try {
            Class.forName("org.h2.Driver");
            conn = DriverManager.getConnection(dbURL, "aleksei", "");
            return conn;
        } catch(ClassNotFoundException e) {
            throw new DAOException("Driver class not found");
        } catch (SQLException e) {
            throw new DAOException(e.getMessage());
        }
    }

    @Override
    public void closeConnection() throws DAOException {
        try {
            if (conn != null) {
                conn.close();
            }
        } catch(SQLException e) {
            throw new DAOException(e.getMessage());
        }
    }
}


