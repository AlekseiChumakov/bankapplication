package src.com.luxoft.bankapp.model;

import com.luxoft.bankapp.Exceptions.AccountExistsException;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Model.SavingAccount;
import org.junit.Before;
import org.junit.Test;

public class ClientTest {
    private Client sut;

    @Before
    public void createNewClient() {
        sut = new Client();
    }

    @Test(expected = AccountExistsException.class)
    public void addAccountThatAlreadyExists() throws AccountExistsException {
        sut.addAccount(new SavingAccount(200.0f, 7));
        sut.addAccount(new SavingAccount(300.0f, 7));
    }
}


