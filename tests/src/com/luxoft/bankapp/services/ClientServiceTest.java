package src.com.luxoft.bankapp.services;

import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;
import com.luxoft.bankapp.test.service.TestService;
import org.junit.Before;
import org.junit.Test;
import java.util.List;
import static org.junit.Assert.*;

public class ClientServiceTest {
    Bank bank;
    Client client1;
    Client client2;
    CheckingAccount account1;
    SavingAccount account2;

    @Before
    public void initBank() throws ServiceException, BankException {
        ServiceFactory.getBankService().clearBank();
        bank = new Bank("My Bank");
        client1 = new Client();
        client1.setName("Mark");
        client1.setGender(Gender.MALE);
        client2 = new Client();
        client2.setName("Maria");
        client2.setGender(Gender.FEMALE);
        account1 = new CheckingAccount();
        client1.addAccount(account1);
        account2 = new SavingAccount();
        client2.addAccount(account2);
        bank.addClient(client1);
        bank.addClient(client2);
        account1.setBalance(0);
        account1.setOverdraftStartValue(1000f);
        account1.setOverdraftCurrentValue(700f);
        account2.setBalance(1000f);
        ServiceFactory.getBankService().save(bank);
    }

    @Test
    public void saveExistingAccountTest() throws Exception {
        Bank bank2 = ServiceFactory.getBankService().getBankByName("My Bank");
        List<Account> accounts1 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank2);
        int numberOfAccounts1 = accounts1.size();
        Account account = accounts1.get(0);

        account.deposit(777f);

        List<Account> accounts2 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank2);
        assertFalse(TestService.isEquals(accounts1, accounts2));

        ServiceFactory.getClientService().saveAccount(client1, new Account[] {account});
        accounts2 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank2);
        int numberOfAccounts2 = accounts2.size();
        assertTrue(numberOfAccounts1 == numberOfAccounts2);
        assertTrue(TestService.isEquals(accounts1, accounts2));
    }

    @Test
    public void saveNewAccountTest() throws Exception {
        Bank bank2 = ServiceFactory.getBankService().getBankByName("My Bank");
        List<Account> accounts1 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank2);
        int numberOfAccounts1 = accounts1.size();
        SavingAccount newAccount = new SavingAccount(333, 0);     //  SavingAccount(balance, id)
        ServiceFactory.getClientService().saveAccount(client1, new Account[] {newAccount});
        List<Account> accounts2 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank2);
        int numberOfAccounts2 = accounts2.size();
        assertTrue(numberOfAccounts1 == numberOfAccounts2 - 1);
        int index;
        for (index = 0; index < accounts2.size(); index++) {
            if (accounts2.get(index).getId() == newAccount.getId()) {
                break;
            }
        }
        assertTrue(index != 0);
        assertTrue(accounts2.get(index).getBalance() == 333);
    }

    @Test
    public void getAndSetActiveAccountTest() throws Exception {
        ServiceFactory.getClientService().setActiveAccount(client1, account1);
        Account account = ServiceFactory.getClientService().getActiveAccount(client1.getName());
        assertTrue(TestService.isEquals(account1, account));
    }

    @Test
    public void getAccountTest() throws Exception {
        List<Account> accounts1 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank);

        SavingAccount newAccount = new SavingAccount(555, 0);
        ServiceFactory.getClientService().saveAccount(client1, new Account[] {newAccount});
        List<Account> accounts2 = ServiceFactory.getClientService().getAccounts(client1.getId(), bank);
        accounts1.add(newAccount);
        assertTrue(TestService.isEquals(accounts1, accounts2));
    }
}

