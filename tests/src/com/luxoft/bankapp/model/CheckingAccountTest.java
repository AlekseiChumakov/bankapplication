package src.com.luxoft.bankapp.model;

import com.luxoft.bankapp.Exceptions.OverDraftLimitExceededException;
import com.luxoft.bankapp.Model.CheckingAccount;
import org.junit.Test;
import static org.junit.Assert.*;

public class CheckingAccountTest {
    private CheckingAccount sut;

    @Test(expected = IllegalArgumentException.class)
    public void createCheckingAccountWithNegativeBalance() {
        sut = new CheckingAccount(-100.0f, 100.0f, 1);
    }

    @Test(expected = IllegalArgumentException.class)
    public void createCheckingAccountWithNegativeOverdraft() {
        sut = new CheckingAccount(100.0f, -100.0f, 1);
    }

    @Test
    public void testDepositIncreaseBalance() {
        sut = new CheckingAccount(100.0f, 100.0f, 2);
        sut.deposit(20.0f);

        assertEquals(120.0f, sut.getBalance(), 0.00001f);
    }

    @Test
    public void testDepositIncreaseBalanceWhenCurrentOverdraftLessThanOverdraft() throws OverDraftLimitExceededException {
        sut = new CheckingAccount(100.0f, 100.0f, 2);
        sut.withdraw(120.0f);
        assertEquals(0.0f, sut.getBalance(), 0.00001f);
        assertEquals(80.0f, sut.getOverdraftCurrentValue(), 0.00001f);
        sut.deposit(110.0f);
        assertEquals(90.0f, sut.getBalance(), 0.00001f);
    }

    @Test(expected = OverDraftLimitExceededException.class)
    public void testWithdrawMoreThanBalancePlusOverdraftThrowsException() throws OverDraftLimitExceededException {
        sut = new CheckingAccount(100.0f, 100.0f, 1);
        sut.withdraw(250.0f);
    }

    @Test(expected = IllegalArgumentException.class)
    public void setNegativeOverdraftTest() {
        sut = new CheckingAccount();
        sut.setOverdraftStartValue(-100.0f);
    }

    @Test
    public void testWithdrawMoreThanBalanceButLessThenCurrentOverdraft() throws OverDraftLimitExceededException {
        sut = new CheckingAccount(100.0f, 100.0f, 1);
        sut.withdraw(150.0f);
        assertEquals(0.0f, sut.getBalance(), 0.00001f);
        assertEquals(50.0f, sut.getOverdraftCurrentValue(), 0.00001f);
    }

    @Test
    public void testWithdrawLessThanBalance() throws OverDraftLimitExceededException {
        sut = new CheckingAccount(100.0f, 100.0f, 1);
        float overdraft = sut.getOverdraftCurrentValue();
        sut.withdraw(50.0f);
        assertEquals(50.0f, sut.getBalance(), 0.00001f);
        assertEquals(overdraft, sut.getOverdraftCurrentValue(), 0.00001f);
    }


}


