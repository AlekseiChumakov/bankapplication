import com.luxoft.bankapp.Exceptions.ClientNotFoundException;
import com.luxoft.bankapp.Exceptions.ServiceException;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;
import com.luxoft.bankapp.Services.ServiceFactory;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;

public class FindClientsServlet extends HttpServlet {
    private final Logger logger = Logger.getLogger(getClass().getName());

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String city = request.getParameter("city");
        String name = request.getParameter("name");
        if (city == null || name == null) {
            logger.warning("City or name is not specified");
            throw new ServletException("City or name is not specified");
        }
        Bank bank = (Bank) getServletContext().getAttribute("bank");
        if (bank == null) {
            response.sendRedirect("/welcome");
            return;
        }
        List<Client> clients = null;
        try {
            if (name.trim().equals("")) {
                if (city.trim().equals("")) {
                    clients = new ArrayList<Client>(ServiceFactory.getBankService().getAllClients(bank));
                } else {
                    clients = ServiceFactory.getBankService().findClientsByCity(bank, city);
                }
            } else {
                Client client;
                client = ServiceFactory.getBankService().getClient(bank, name);
                if (client != null) {
                    if (city.trim().equals("") || client.getCity().equals(city)) {
                        clients = new ArrayList<Client>();
                        clients.add(client);
                    }
                }
            }
        } catch (ClientNotFoundException e) {

        } catch (ServiceException e) {
            logger.log(Level.SEVERE, "Bank service error", e);
            throw new ServletException("Bank service error");
        }
        request.setAttribute("clients", clients);
        request.getRequestDispatcher("/findclient.jsp").forward(request, response);
    }
}
