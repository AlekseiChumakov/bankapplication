<!DOCTYPE html>
<html>
<head lang="en">
    <link rel="stylesheet" href="static-content/bank.css">
    <script src="static-content/jquery-1.11.0.min.js" type="text/javascript"></script>
    <script type="text/javascript" src="validation.js"></script>
    <%@ page language="java" pageEncoding="utf-8" contentType="text/html;charset=utf-8"%>
    <%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
    <title>Find client</title>
</head>

<body>
<h1>Поиск клиента:</h1>
<br><br>
<form action="/findclients" method="POST">
    Город:<input type="text" name="city">
    Имя:<input type="text" name="name">
    <input type="submit" value="Find">
</form>
<br><br>
<table id="searchingResults"><tr>
    <th>Город</th>
    <th>Имя клиента</th>
    <th>Баланс</th>
    <c:forEach var="client" items="${clients}" varStatus="status">
    <tr><td><c:out value="${client.city}"/></td>
    <td><a href="/client?id=${client.id}"><c:out value="${client.name}"/></a></td>
    <td>
    <c:choose>
        <c:when test="${empty client.activeAccount}"><c:out value="-"/></c:when>
        <c:otherwise>
            <c:out value="${client.activeAccount.balance}"/>
        </c:otherwise>
    </c:choose>
   </td>
    </c:forEach>
    </tr>
</table>
</body>
</html>


