package com.luxoft.bankapp.Services;

import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.CheckingAccount;
import com.luxoft.bankapp.Model.Client;

import java.util.*;

public class BankReport {
    public int getNumberOfClients(Bank bank) {
        return bank.getClients().size();
    }

    public int getAccountsNumber(Bank bank) {
        int n = 0;
        for (Client c: bank.getClients()) {
            n += c.getAccounts().size();
        }
        return n;
    }

    public Set<Client> getClientsSorted(Bank bank) {
        Set<Client> set = new TreeSet(new Comparator() {
            public int compare(Object o1, Object o2) {
                int i =  Float.compare(((Client) o1).getSummaryBalance(), ((Client) o2).getSummaryBalance());
                if (i == 0) {
                    return ((Client) o1).getName().compareTo(((Client) o2).getName());
                } else {
                    return i;
                }
            }
        });
        set.addAll(bank.getClients());
        return set;
    }

    public double getBankCreditSum(Bank bank) {
        double result = 0;
        for (Client c : bank.getClients()) {
            for (Account a : c.getAccounts()) {
                if (a instanceof CheckingAccount) {
                    result += ((CheckingAccount) a).getCredit();
                }
            }
        }
        return result;
    }

    public Map<String, List<Client>> getClientsByCity(Bank bank) {
        Map<String, List<Client>> map = new TreeMap<String, List<Client>>();
        for (Client c: bank.getClients()) {
            String city = c.getCity().toUpperCase();
            if (!map.containsKey(city)) {
                map.put(city, new ArrayList<Client>());
            }
            map.get(city).add(c);
        }
        return map;
    }
}









