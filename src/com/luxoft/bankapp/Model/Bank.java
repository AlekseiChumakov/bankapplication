package com.luxoft.bankapp.Model;

import com.luxoft.bankapp.Annotations.NoDB;
import com.luxoft.bankapp.Exceptions.BankException;
import com.luxoft.bankapp.Exceptions.ClientExistsException;
import com.luxoft.bankapp.Exceptions.FeedException;
import com.luxoft.bankapp.test.service.BankObject;

import java.util.*;

public class Bank implements Report, BankObject {
    @NoDB private int id;
    @NoDB private List<ClientRegistrationListener> listeners;
    @NoDB private Map<String, Client> clientsNames;
    private String name;
    private Set<Client> clients;

    public Bank() {
        clients = new HashSet<Client>();
        clientsNames = new HashMap<String, Client>();
        listeners = new ArrayList<ClientRegistrationListener>();
    }

    public Bank(String n) {
        this();
        name = n;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public Bank(List<ClientRegistrationListener> list, String n) {
        this(n);
        listeners = list;
        listeners.add(new ClientRegistrationListener() {
            public void onClientAdded(Client c) {
                System.out.println("Client: " + c.getClientSalutation() + " " + (new Date()));
            }
        });
        listeners.add(new ClientRegistrationListener() {
            public void onClientAdded(Client c) {
                clientsNames.put(c.getName(), c);
            }
        });
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Map<String, Client> getClientsNames() {
        return Collections.unmodifiableMap(clientsNames);
    }

    @Override
    public void printReport() {
        System.out.println("Welcome to " + name);
        System.out.println("");
        for (Client c : clients) {
            c.printReport();
        }
    }

    public void addClient(Client c) throws ClientExistsException {
        if (!clients.add(c)) {
            throw new ClientExistsException();
        }
        for (ClientRegistrationListener l : listeners) {
            l.onClientAdded(c);
        }
    }

    public void removeClient(Client c) {
        clientsNames.remove(c.getName());
        clients.remove(c);
    }

    public Set<Client> getClients() {
        return Collections.unmodifiableSet(clients);
    }

    public void setClients(Set<Client> clients) {
        this.clients = clients;
    }

    public static class PrintClientListener implements ClientRegistrationListener {
        @Override
        public void onClientAdded(Client c) {
            System.out.println("Client " + c.getClientSalutation() + " is added");
        }
    }

    public static class EmailNotificationListener implements ClientRegistrationListener {
        @Override
        public void onClientAdded(Client c) {
            System.out.println("Notification email for client " + c.getClientSalutation() + " to be sent");
        }
    }

    public void parseFeed(Map<String, String> feed) throws BankException {
        String name = feed.get("name");
        Client client = clientsNames.get(name);
        boolean clientExists = true;
        if (client == null) {
            clientExists = false;
            client = new Client(name);
            client.setCity(feed.get("city"));
            client.setTelephone(feed.get("telephone"));
            client.setEmail(feed.get("email"));
            String s = feed.get("gender");
            if (s == null) {
                throw new FeedException("Error reading gender");
            }
            Gender g;
            if (s.equalsIgnoreCase("m")) g = Gender.MALE;
            else if (s.equalsIgnoreCase("f")) g = Gender.FEMALE;
            else throw new FeedException("Error reading gender");
            client.setGender(g);
        }
        client.parseFeed(feed);
        if (!clientExists) {
            addClient(client);
        }
    }
}








