package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.DAOException;
import com.luxoft.bankapp.Model.Account;
import com.luxoft.bankapp.Model.Bank;
import com.luxoft.bankapp.Model.Client;

import java.util.List;

public interface AccountDAO {
    void removeByClientId(int id) throws DAOException;
    List<Account> getClientAccounts(int clientId, Bank bank) throws DAOException ;
    Account getActiveAccount(String clientName) throws DAOException;
    void setActiveAccount(Client client, Account account) throws DAOException;
    boolean ifExists(Account account) throws DAOException;
    void save(Account[] accounts, Client client) throws DAOException;
}



