package com.luxoft.bankapp.DAO;

import com.luxoft.bankapp.Exceptions.DAOException;

import java.sql.Connection;

public interface BaseDAO {
    public Connection openConnection() throws DAOException;
    public void closeConnection() throws DAOException;
}


