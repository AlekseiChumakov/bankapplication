package com.luxoft.bankapp.Main;

import com.luxoft.bankapp.Commands.*;
import com.luxoft.bankapp.Exceptions.*;
import com.luxoft.bankapp.Model.*;
import com.luxoft.bankapp.Services.ServiceFactory;

import java.util.*;

public class BankCommander {
    private final static String bankNameByDefault = "Bank №1";
    public static Bank currentBank;
    private Client currentClient;
    private Map<String, Command> commandsTable = new TreeMap<String, Command>();

    static {
        try {
            currentBank = ServiceFactory.getBankService().getBankByName(bankNameByDefault);
        } catch (ServiceException e) {
            System.out.println("Service error! " + e.getMessage());
            System.exit(0);
        }
        if (currentBank == null) {
            System.out.println("Bank \"" + bankNameByDefault + "\" not found");
            System.exit(0);
        }
    }

    public static final String fromConsole(String prompt) {
        StringBuilder sb = new StringBuilder();
        Scanner scanner = new Scanner(System.in);
        while (sb.length() == 0) {
            System.out.print(prompt);
            sb.append(scanner.nextLine().trim());
        }
        return sb.toString();
    }

    public void registerCommand(String name, Command command) {
        commandsTable.put(name, command);
    }

    public void removeCommand(String name) {
        commandsTable.remove(name);
    }

    public Map<String, Command> getCommandsTable() {
        return commandsTable;
    }

    public Client getCurrentClient() {
        return currentClient;
    }

    public void setCurrentClient(Client c) {
        currentClient = c;
    }

    public static void main(final String args[]) {

        BankCommander commander = new BankCommander();
        commander.registerCommand("01", new FindClientCommand());
        commander.registerCommand("02", new GetAccountsCommand());
        commander.registerCommand("03", new TransferCommand());
        commander.registerCommand("04", new WithdrawCommand());
        commander.registerCommand("05", new DepositCommand());
        commander.registerCommand("06", new AddClientCommand());
        commander.registerCommand("07", new ReportCommand());
        commander.registerCommand("08", new AddAccountCommand());
        commander.registerCommand("09", new SetActiveAccountCommand());
        commander.registerCommand("10", new RemoveClientCommand());
        commander.registerCommand("11", new ExitCommand());

        for (String s : commander.commandsTable.keySet()) {
            System.out.print("\"" + s + "\" -> ");
            commander.commandsTable.get(s).printCommandInfo();
            System.out.println("");
        }

        Scanner scanner = new Scanner(System.in);
        StringBuilder sb = new StringBuilder();

        while (true) {
            sb.delete(0, sb.length());
            while (sb.length() == 0) {
                System.out.println("Input command name: ");
                sb.append(scanner.nextLine().trim());
            }
            if (!commander.commandsTable.containsKey(sb.toString())) {
                System.out.println("Error! Command name not found.");
                System.out.println("");
                continue;
            }
            System.out.print("Executing command \"");
            commander.commandsTable.get(sb.toString()).printCommandInfo();
            System.out.print("\" ...\n");
            try {
                commander.commandsTable.get(sb.toString()).execute(commander);
            } catch (ServiceException e) {
                System.out.println("Service error! " + e.getMessage());
                continue;
            } catch (ClientExistsException e) {
                System.out.println("Error! This client already exists\n");
                continue;
            }  catch (OverDraftLimitExceededException e) {
                System.out.println("Error! Overdraft limit exceeded\n");
                continue;
            } catch (NotEnoughFundsException e) {
                System.out.println("Error! Not Enough Funds\n");
                continue;
            } catch (AccountExistsException e) {
                System.out.println("Error! An attempt to add an existing account!");
                continue;
            } catch (ClientNotFoundException e) {
                System.out.println("Error! Client with that name doesn't exists.");
                continue;
            } catch (BankException e) {
                continue;
            }
        }
    }
}









